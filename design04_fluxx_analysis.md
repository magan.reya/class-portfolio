# Fluxx Objects Design
## Reya Magan


### Tracking Changes

#### Changes needed in order to support Pirate Fluxx version
* new classes: Surprise
* new method in class: The Keeper class will have a checkCategory method to see if it falls into a booty or ship category
* code logic change: I think the gamePlay class can work in a similar fashion as they browser model class in the Browser project. It will be passed as an instance for each member -> and each card will be kept track of throughout all of the different card classes.



### CRC Card Classes (Updated)



1. This class's purpose or value is to represent a Surprise. This will be inherited from the Action card method (each relevant method will be overridden) as Surprise cards usually require an action.

   |Surprise| |
   |---|---|
   |**Change_Keepers Method:** *parameters* = (int) number_of_players, (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile; *return* = (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile        |Game_Setup|
   |**Change_Goals Method:** *parameters* = (pair) Current_Goal, (int) number_of_players, (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile; *return* = (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile   | Goals |
   |**Change_Rules Method** *parameters* = (pair) Basic_Rule, (int) number_of_players, (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile; *return* = (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile   | Keeper |
   |**Change_Cards Method:** *parameters* = (int) number_of_players, (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile; *return* = (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile| Rules |
   |**Repeat_Turn Method:** *parameters* = (int) number_of_players, (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile; *return* = (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile ||
   |**Rotate_Players Method:** *parameters* = (int) number_of_players, (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile; *return* = (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile||
   |**Public Void Read_Surprise** *parameters* = (HashMap) Surprise_Card ||
   |Private Goal Goal_Card ||
   |Private Keeper Keeper_Card ||
   |Private Rules Rules_Card ||
   |Public Variable (Pair) Basic_Rule ||
   |Public Variable (Pair) Current_Rule ||
   |Public Variable (Pair) Current_Goal ||
```java
public class Surprise{
    //When an Surprise card is drawn/used, this class deals with the possible outcomes
   public void Read_Surprise(HashMap<Integer, String> Surprise_Card){
       //This is the method that the another class will call, this method will then call one of the necessary methods below depending on what the Surprise is.
   }
   @Override
   public ArrayList<HashMap<Integer,String>> Change_Keepers (int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> cards_in_hand, HashMap<Integer,String> discard_pile){
        // If the Surprise card has anything to do with the Keeper card (Exchange Keepers, Steal a Keeper, etc.) then this method will call the Keeper class with the Keeper_Card object.
    }
    @Override
   public ArrayList<HashMap<Integer,String>> Change_Goals (Pair<Integer, String> Current_Goal, int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> cards_in_hand, HashMap<Integer,String> discard_pile){
        // If the Surprise card has anything to do with the Goal card, this method will call the Goals class using the Goal_Card object
    }
    @Override
   public Arraylist<HashMap<Integer, String>> Change_Rules (Pair<Integer, String> Basic_Rule, int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> cards_in_hand, HashMap<Integer,String> discard_pile){
        // If the Surprise card has anything to do with the Rules card (Trash a New Rule, Rules Reset, etc.) then this method calls the Rules class using the Rules_Card object
    }
    @Override
   public ArrayList<HashMap<Integer, String>> Change_Cards (int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> cards_in_hand, HashMap<Integer,String> discard_pile){
        //If the Surprise card has something to do with card, but not a specific type (Draw 2 and Use 'Em, Zap a Card!, etc.) then this method will just deal with that itself
    }
    @Override
   public ArrayList<HashMap<Integer, String>> Repeat_Turn (int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> cards_in_hand, HashMap<Integer,String> discard_pile){
        //If the Surprise card has the player repeat a turn (Take Another Turn), then this method will do so
   }
   @Override
   public ArrayList<HashMap<Integer, String>> Rotate_Players (int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> cards_in_hand, HashMap<Integer,String> discard_pile){
        //If the Surprise card has players rotate cards (Rotate Hands) then this method will do so.
   }
}
```



### Game Loop (Updated)

* After setting up the game, turns proceed like this:
    1. Draw the number of cards currently required (start with Draw 1)
    2. Play the number of cards currently required (start with Play 1)
    3. Discard to comply with any Limit rules in play (start with no limit)
    4. The game ends when a player meets the conditions of a current Goal card.
    5. If a player draws a Creeper card, they must place it front of them and complete any instructions immediately
    6. If a player draws an UnGoal card, this can overwrite the Goal card essentially such that if any player meets an UnGoal condition the zombies win.
    7. If a player draws a Surprise card, this will deal with whatever action this card requires -> it will communicate with the needed classes


### Use Cases (Updated)

* A player plays a Goal card, changing the current goal, and wins the game.
  * This means that the player will go into the "Goal" class, and replace the current goal, this will then call the Keeper class, and after looking through the player's current Keeper cards, a match would be found between them and the new Goal card -> causing them to win.


* A player plays a Rule card, adding to the current rules to set a hand-size limit, requiring all players to immediately drop cards from their hands if necessary.
  * This Rule change will recognize that a hand-size limit means that it needs to use its Change_Cards method - It will go through each player and make them remove the necessary number of cards before resuming play.


* A player plays a Rule card, changing the current rule from Play 1 to Play All, requiring the player to play more cards this turn.
  * This Rule change will also call the Change_Cards method, to prompt the player to play all of their cards. If a specific card is played here - say, an action card - then that class and its relevant methods will also be called.


* A player plays an Action card, allowing him to choose cards from the discard pile and play them.
  * This will call the Action class, specifically the Change_Cards method. The player will gain more cards, and then play them. Whatever card they play (for example a Keeper card), the respective class and its methods will be called to process it.


* A player plays an Action card, allowing him to choose cards from another player's hand and play them.
  * This will call the Rotate_Players method in the Action class to take those cards from another player and then play them. Whatever cards are played, those classes and methods will also be called and processed.

### Use Cases (New)

* Zombie card 1 - Brain Baseball Goal
  * If you have already drawn certain Keepers or Creepers (Baseball bat and Zombie), and happen to also have a Baseball keeper, if this Goal card is drawn you would automatically win as you meet the requirement of 1 baseball bat, a brain, and a zombie. This would use the Check_Keeper and Check_Creeper methods in their respective classes, and compare them to the Current_Goal variable

* Zombie card 2 - Zombie Victory UnGoal
  * If there are 4 zombies on the table, and one player plays another zombie, if someone has this card the zombies will automatically win. This would utilize the Check_Creeper method from the Creeper class as well as the Current_UnGoal variable to find whether they are equal - if so, the GameIsWon variable will return true for zombies.

* Zombie card 3 - Larry Creeper
  * If this card is drawn, then the player cannot win no matter what the Goal says. This would be part of the Check_Creeper method in the Creeper class. If it is true that a player has this card, even if the current goal (Read_Goal from Goals class) allows for a win with zombies, this will be overwritten. An action card that causes Creepers to be changed (Action class, Change_Creepers method) would need to be used to officially get rid of this card and be able to win again.

* Zombie card 4 - Return of the Dead Action
  * All of the Creepers that have been discarded will be drawn in the Action class (Change_Creepers method) and be dealt to players using the Rotate_Players method. The players will then place these Creeper cards out on the table and if at this point a goal or ungoal is met the game would end - otherwise it continues on.

* Surprise card 1 - Canceled Plans Surprise
  * First the Goals class will be called (specifically the Change_Goal method). During your turn, the current Goal needs to be discarded so this will happen, and all the other player instances will be called to discard a Goal card they have too. Then, out of your turn, if another player instance sets a goal card, the Goals class and ChangeGoal method will be called to discard it.

* Surprise card 2 - That Be Mine! Surprise
  * This is similar to the Canceled Plans surprise, but it will use the Keeper class and the changeKeepers method. During you turn, another player instance will be called and their Keeper will be given to you. Out of your turn, if another player plays a Keeper, the class and method will be called again to steal their Keeper.

 * Overall design goals (i.e., what changes are you trying to easily support, what details are you trying to hide)


 * Design decision (i.e., to make a class or not, a method or not, an abstraction or not)
   * Principled Justification:
   I feel like my design was pretty modular. I had a different class for each card, and for cards with similar behaviors I thought
   to use inheritance to set those up. I also thought to make an overall gamePlay class to set up an instance of each player. This way, each player can go through the game and its classes/methods.
     * Don't Repeat Yourself (DRY):
     I created inheritance for classes with similar logic (Creeper and Keeper, Surprise and Action). This is because these cards have a similar behavior, so one parent class can dictate a lot of their behavior.
     * Tell, Don't Ask:
     I think I did okay here, I have independent classes that can be called as needed (depending on which card is used).
     * Loose Coupling:
     Only the player class (gamePlay) is dependent on any other class - the rest are basically independent card classes.
     * Single Responsibility Principle:
     Since each class is based on a card type, their only responsibility is to carry out the functions of the card. So, I think this is set up well in my code.
     * Open/Closed Principle:
     I think this is pretty good, when the new cards were added I didn't need to change any other classes or methods. So I feel like it is pretty open/closed.