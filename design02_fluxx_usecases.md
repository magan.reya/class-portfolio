# Fluxx Objects UseCases
## Reya Magan

### High Level Design Ideas 
Something that I noticed immediately when thinking of my use cases was that my original design did not include a variable for the amount of cards each player needs to draw at the start of the game. The first thing I would change thus is that I would create a public variable that holds the number 3 - as that is how many cards a player starts out with. Each player also has to draw x cards and play x cards every turn dependent on the Basic Rule card, so I would make a boolean that checks to make sure that each player takes that course of action during their turn.

### Use Cases
Here is an image of what the typical game play would look like:
![](imgs/overallGamePlay.jpeg)
The game would basically travel between each class as different cards are drawn, and each player would have their own hand
of cards to keep track of. I think it would be helpful to actually make a player class and make each player an object and then go around the card classes using player.CardsInHand to keep track
of a player's cards. This game would basically continue until a match would be found between the goal card and someone's keeper cards.

Here is an example of one scenario in which the game could be played:
![](imgs/useCaseExample.jpeg)
So here we have two players. The first player draws three cards as Keeper ("moon"), Action ("Change goal"), and Keeper ("sun"). The second player draws a Goal
("moon", "cloud"), an Action ("change Keeper"), and a Keeper ("Rain"). The basic rule is to draw a card and place a card so player one takes their turn and draws a rule card that sets the keeper limit as 2. This becomes the global rule for all the players.
Then player 2 draws a new Keeper ("snow") card and plays the Goal card which sets the Goal as "moon", "cloud". Then player one goes again and draws a Goal "moon", "sky" card and plays the Action "change Goal" card. This sends the player into the Action class where they change 
the global goal card to now be "moon", "sky". Then player three draws a card, which is a keeper - since they now have 3 keepers they must get rid of a keeper (through the keeper class). Player 1 goes
again and draws the keeper "sun" card. They have to get rid of one keeper as per the rule, and then their two keepers are sent into the keeper class, where a match is found with the current goal card and player one wins the game.
This is just one example of how the game of Fluxx will work within all of the card classes. So long as each class contains the relevant information about its respective card, this diagram is how the game play will usually go.
For example, the Rule card may have been "Play all" instead of "Keeper Limit 2", which would have made all the players play every card (and go through the required card classes as they are called). Some players would possibly have 0 cards and have to draw on their next turn following that. Or perhaps, the Action card could have been "Draw from Discard" or "Take from Player". These would work the same way, for the discard card the global discard pile would transfer its first index card to the player who
played the Action card, and then the player would play that card and go through the necessary methods. If the player used the take a card Action, then they would communicate with the other player's hands in deck object and take the first card, and then play that and go through the relevant methods.