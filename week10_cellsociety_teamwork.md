# Journal : Cell Society Team Review
## Team Number: 9
## Name: Reya Magan
## Date: 11/2


### Contributions and Roles

* Describe your role(s) and your satisfaction
My Role: Involved with the backend. Implemented the Schelling and WaTor games, and pair programmed on the GameOfLife game.
  Also implemented neighbor policies, as well as random cell configuration. Pair programmed the rule set up. Created the parsing mechanisms for the .sim and .csv files, and used reflection for both classes and methods to call the relevant ones.
  Also refactored a lot to include consumers + abstract wherever possible. Refactored to make sure that instance variables were not accessed directly. Implemented error handling for the back-end. Implemented method reflection within
  the rule structure to call relevant methods within Schelling and Fire games to set percent parameters.

I was pretty happy with my work, I think I used a lot of relevant concepts from class and was able to utilize them well too.
* Describe your team mate(s) role(s) and their satisfaction
Amr was also on the backend with me, and he was great. He worked on a lot of really interesting property files that allowed us to use reflection to call
different rule type classes. I think he should be really proud of himself.

Evelyn and Luke worked on the view. They implemented a lot of features like the graphs, different cell shapes, and different colors. I think it looked
really great!

### Team Events

* Describe when you felt the most satisfaction

I took on the challenge of completing the Wa-Tor game, which
was really difficult. It took me over 4 days to get it right,
and it was an extremely painful process. However, I learned a lot, 
and I implemented consumers, reflection, and interfaces in designing
the game. I was really proud of myself for creating the game at full functionality,
and also using design principles during it.

* Describe when you felt the team made the biggest leap ahead

I think that Amr's rule properties files were really genius and they allowed
us to have an almost completely abstract rules class which was really cool. It allowed us to add new games
without ever having to change the rules, we just changed the parameters that were passed in.

* Describe way(s) the team has worked well

I think the two sub-teams (backend and frontend) worked really well within the subgroups.
The backend team was on the same page, as was the frontend team. We had strong design discussions
and decisions within each team, and were able to create a strong product as a result.
* Describe problem(s) with the way the team worked together

There was a pretty big disconnect between the frontend team and the backend team. I really did try to connect with the front-end team,
but they were often unresponsive. This became really stressful as a huge part of our program crashed due to a merge conflict at the last minute.
Amr and I ended up spending hours fixing it, but did not really get a response from the other teammates.


### Learning Opportunities

* Your biggest strength as a team member

I work really hard on my own work, and am always willing to help wherever it is needed.
* Your biggest weakness as a team member

I struggle with being assertive, I do not want to come across as "bossy" or overbearing, 
so at some point when I realized that I wasn't getting much of a response I stopped pushing for it.
I don't think I should have done this, I think being more assertive would have probably allowed for less
merging issues in the end.

* What you have learned team collaboration/communication during this project

Communication is SO SO important. The last few hours of this project were some of the most stressful I have ever had.
If I could go back and redo this project I would make sure that everyone was on the same page at all times.
It was extremely overwhelming for me when everything broke, and I don't want to experience something
like that again.

* A specific thing can you do to make team collaboration/communication more productive

I think I need to stop worrying that I may come across negatively. If it is for the good of the group in 
the long run, I think it is better to push for more communication rather than get caught up on individual tasks