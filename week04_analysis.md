# Journal : Improvement Analysis
### Reya Magan


## Looking Back

### Practice of coding

#### What are three things you think about differently when coding because of this course?
1. I try to split up similar methods into their own classes (visual all in one, modular in the other)
2. I try to use classes efficiently to keep my code readable and concise
3. I try to keep my methods are efficient and concise as I can


#### What is a coding habit you have changed for the better because of this course?
I used to write everything in one succession, so I would have thousands of lines of code in one file and that would make up the entire project. Now, I feel like I am more organized in setting up my code and 
I utilize multiple classes to keep repetition low while making my code readable.

#### What is a feature from any of the exercise specifications you felt was made easier to implement by the code's design and why?
I think for the most recent project, browser, I benefited a lot from splitting up the visual components from the backend. Having the visual class take in the instance of the browser model allowed me to easily add new features as needed, since I could easily access the information regarding the instance of my browser. This will be really helpful for later projects.


### Design issue
A design issue that I encountered was in using inheritance for the maze project, specifically the RandomWalk class. Because it was so different from the rest of the classes, I was not sure how to refactor it to really use the parent class much.
#### What trade-offs were you considering?
Because I was struggling to use inheritance for the random walk class, I considered keeping its step method completely intact and just overriding it so that the parent class would not get involved. I thought that this could possibly make my design a little less clean than if the parent class was involved, but everything would work well.

#### What alternatives did you consider?
I did try utilizing the parent class more, but this resulted in a lot of bugs and errors that just kept making my code messier when I tried to fix them. I ended up keeping it pretty separate from the parent class, then.

#### Why did you make the final decision you did?
When I utilized the parent class step method, the amount of bugs forced me to add a lot more code that just made it messier in general, so I felt that though there was a slight design flaw in keeping the random walk class separate, for me personally it was cleaner than if I had used the parent class more for it.


### Actively working to improve

#### What is a coding habit you are still working on changing?
I am still struggling a lot with inheritance, I need to take some time away to really focus and understand it better because right now I get very confused and stuck easily.

#### What part(s) of the code did you spend the most time on?
In general I think I find refactoring code from lab to be the most difficult, especially for maze I spent a lot of time trying to figure out inheritance but ended up not really getting anywhere good with it.

#### What is something not yet addressed that you want to know more about or do better?
It would be cool to learn about how tests work for visual classes.


### Transition Feelings

#### How have you felt having your code reviewed? Is the feedback clear and is it making a positive impact?
The feedback from my TA has been really good, since I am struggling with inheritance, she has diagrammed it out for me in order to help me understand. I really appreciate her guidance.

#### How has the act of refactoring your code felt? Are the goals clear and is it making a positive impact?
For the most part, I feel that I have improved in refactoring my code. I am able to split my code into relevant classes and use instances of classes to communicate through my program. I think that I really need to improve on inheritance still, though, as that is something I feel I am still lacking in.
#### How have you felt thinking about and designing a program without actually writing any code?

* What was hard?
For the fluxx exercise where I designed a program but did not write any code, the hardest part was just getting started. It took me some time to understand how the game worked and how I could 
cleanly separate out the play into separate classes. Once I developed a model to separate each card type out into a different class, I felt that I was able to add new changes in much simpler manner.
* What was easy? Once I had the skeleton of my design set up, it was not too hard to continue to add more features or go through use cases.




### Overall, how do you feel you are doing in the course?
I feel like I am doing decently well in the course, I think I have improved a lot, but still have much to improve on.
#### How could you improve your learn choices?
I definitely have been trying to "go around" things that cause me a lot of confusion, I think I just feel very stressed for time so sometimes I make some decisions that aren't necessarily the best design.

#### How could the course be improved to better help meet your needs?
I think I just feel very rushed every weekend, it might be me personally, but it takes me some time to think through new concepts, so having 3/4 days to do a project becomes really stressful so I'm not sure if I am doing the best I can.



## Looking Ahead

#### Describe a good team experience you have had. What is something about that experience you can apply to create positive teams in the future?
I worked on a Bass Connections team in which every member had very different backgrounds. I thought it would be difficult to work together at first, but these
different backgrounds actually helped us to see multiple perspectives when designing our project. I think being open to different ideas and perspectives will
allow my future teams to work well together toward a strong solution.

#### Describe a bad team experience you have had. What is something about that experience you can apply to create positive teams in the future?
In my freshman year engineering design course, there was a power struggle of sorts between two members. It was hard to get work done because they would
often disagree with each other, so I think that in team based things everyone should be more equally involved rather than having someone with more "power".

#### What are ways people can show they respect each other's ideas and work?
I think people should be receptive to all ideas and respectful when talking about someone's work. Constructive criticism is a good way
to provide advice on changes to be made without making the team member feel less.

#### What are ways people can show they are committed to the team's success?
Showing up to meetings, actively responding to questions, and providing advice when it is needed.

#### What are ways people can be proactive in a team?
Getting their part done early, offering to help others, asking questions when they arise.

#### How can communication during meetings help or hurt your team experience?
If the communication is about a topic currently being discussed, it can help toward finding a solution. However, any unrelated communication can set the team off course and off task.


#### How can communication beyond meetings help or hurt your team experience?
If there are questions, communication can help to address those. Questions that can be answered with a simple
Google search, however, can take away too much time.

#### How can the size and timing of your GIT commits help or hurt your team experience?
If you are working on a certain part of code and you commit just that it with a good message the team can easily understand the changes. However, large changes with only one push are very confusing to understand since so many things have been changed. As for timing, it's good to work in different branches so merge conflicts don't happen.