package ooga.reflection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import ooga.components.componentAPI.ImmutableGrid;
import ooga.model.Model;
import ooga.players.Player;
import ooga.reflection.reflectionAPI.ReflectionSetUpAPI;
import ooga.resources.Config;
import ooga.view.GridView;
import ooga.view.game.GameView;

/**
 * This class is used with the ReflectionFactory class to set up the relevant parameters within
 * each reflection task. It is well designed because it separates out the set up functionality
 * and can be called any time reflection is to be initiated.
 */
public class ReflectionSetUp extends ReflectionFactory implements ReflectionSetUpAPI {

  /**
   * Sets up players with playerTypes
   */
  public List<Player> setUpPlayers(List<String> playerNames, List<String> playerTypes) {
    assert (playerNames.size() == playerTypes.size());
    List<Player> players = new ArrayList<>();

    for (int i = 0; i < playerNames.size(); i += 1) {
      Class[] playerConstructorClasses = {int.class, String.class};
      Object[] playerConstructorValues = new Object[]{i + 1, playerNames.get(i)};
      Player player = (Player) getClassReflection(
          Config.DEFAULT_PLAYER_REFLECTION_PATH,
          playerConstructorClasses,
          playerConstructorValues,
          playerTypes.get(i));
      players.add(player);
    }
    return players;
  }

  /**
   * Reflection for setting up model
   **/
  public Model setUpModel(String gameType, int[][] states) {
    Class[] modelConstructorClasses = {int[][].class};
    Object[] modelConstructorValues = {states};
    Model model = (Model) getClassReflection(
        Config.DEFAULT_MODEL_REFLECTION_PATH,
        modelConstructorClasses,
        modelConstructorValues,
        gameType);
    return model;
  }

  /**
   * Setting up the game view of different games using reflection
   */
  public GameView setUpGameView(String gameType, String language, String mode, ImmutableGrid grid,
      Player currPlayer, List<Player> players, List<String> playerTypes) {
    Class[] gameViewConstructorClasses = {String.class, String.class, String.class,
        ImmutableGrid.class, Player.class, List.class};
    Object[] gameViewConstructorValues = {gameType, language, mode, grid, currPlayer, players};
    StringBuilder str = new StringBuilder();

    // Creating a new duplicate list of playerTypes, then sorting it, so we make sure that we don't
    // encounter the error of CPUHumanGameView, which is not an actual class.
    List<String> playerTypesSorted = new ArrayList<>(playerTypes);
    Collections.sort(playerTypesSorted, Comparator.reverseOrder());
    for (String s : playerTypesSorted) {
      str.append(s);
    }
    GameView gameView = (GameView) getClassReflection(
        Config.DEFAULT_GAMEVIEW_CONSTRUCTOR_PATH,
        gameViewConstructorClasses,
        gameViewConstructorValues,
        str.toString());
    return gameView;
  }

  @Deprecated
  public GridView setUpGridView(String gameType) {
    GridView gridView = (GridView) getClassReflection(
        Config.DEFAULT_GRIDVIEW_REFLECTION_PATH,
        null,
        null,
        gameType);
    return gridView;
  }
}
