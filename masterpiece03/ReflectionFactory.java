package ooga.reflection;


import ooga.reflection.reflectionAPI.ReflectionAPI;
import ooga.resources.Config;

import java.beans.PropertyChangeEvent;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.function.Consumer;
import static ooga.Main.LOGGER;

/**
 * This class is a factory class that deals with different types of reflections throughout the project.
 * To make code more readable, whenever reflection is used this factory is called to do the reflection to
 * initialize the correct class or for non-class related reflections to set up the reflection and invoke/set methods
 * or fields as necessary. It is good design because it separates out all the reflection functionality to within one
 * central factory. It also uses consumers to break up what otherwise would be large if-else blocks.
 */
public class ReflectionFactory implements ReflectionAPI {

    /**
     * This method is called whenever class reflection is needed. When we need to call an extended class,
     * we can pass in the corresponding parameters to this method which will setup the correct class with its
     * constructors.
     * @param config : This is the path to the class that we want to use reflection to setup. This path is taken
     *               from the Config file, and combined with the specific type of class object that we would like
     *               to set up. For example, if we want to play Gomoku, we need to intialize the GomokuModel,
     *               so we would pass in the path to the Model class, and then combine it with the game type (Gomoku)
     *               to set it up.
     * @param classParams: These are the parameter class types found in the constructor of the class to be initialized.
     * @param classValues: These are the values to be passed into the constructor of the class to be initialized.
     * @param type: This is the type of specific extended class that we want to set up. For example, "Gomoku" in the case
     *            of setting up the GomokuModel
     * @return the class that is being initialized as a result of the passed in type (eg. GomokuModel, CPUPlayer, etc.)
     */
    public Object getClassReflection(String config, Class[] classParams, Object[] classValues, String type)  {
        try {
            return Class
                    .forName(String.format(config, type))
                    .getConstructor(classParams)
                    .newInstance(classValues);
        }
        catch (Exception e){
            LOGGER.error(e.toString());
        }
    return null;
    }

    /**
     * This method is called whenever we need to use reflection for something non-class related. This could
     * be for Method, PropertyChange Events, or Fields. When this method is called, a consumer is used to read
     * in the type of reflection to be done, and the corresponding method is called to complete the reflection as
     * well as any invoking or setting that needs to be done.
     * @param type: The type of reflection to be done. The consumer map has this as a key, and sends the corresponding
     *            value to the consumerGenerateReflection() method to call the correct method to complete the reflection.
     *            For example, if the type of reflection is "Field" then the getFieldReflection() method is called
     *            with the correct parameters.
     * @param currentClass: The class in which reflection is happening
     * @param name: The method or field name to get
     * @param toActOn: The object to act on/invoke
     * @param value: The object that defines what to invoke/set
     */
    public void getNonClassReflection(String type, Class<?> currentClass, String name,
                                        Object toActOn, Object value){
        Map<String, Consumer<String>> reflectionMap = Map.of(Config.REFLECTION_FACTORY_METHOD_REFLECTION,
                String -> getMethodReflection(currentClass, name, toActOn),
                Config.REFLECTION_FACTORY_PROPERTY_REFLECTION,
                String -> getPropertyChangeReflection(currentClass, name, toActOn, value),
                Config.REFLECTION_FACTORY_FIELD_REFLECTION,
                String -> getFieldReflection(currentClass, name, toActOn, value));
        consumerGenerateReflection(type, reflectionMap.get(type));
    }

    private void getPropertyChangeReflection(Class<?> currentClass, String evtName, Object toInvoke, Object value){
        try {
            Method method = currentClass.getDeclaredMethod(evtName,
                    PropertyChangeEvent.class);
            method.setAccessible(true);
            PropertyChangeEvent evt = (PropertyChangeEvent) value;
            method.invoke(toInvoke, evt);
            method.setAccessible(false);
        }
        catch (Exception e){
            LOGGER.error(e.toString());
        }
    }

    private void getFieldReflection(Class<?> currentClass, String fieldName, Object toSet, Object value){
        try {
            Field field = currentClass.getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(toSet, value);
            field.setAccessible(false);
        }
        catch (Exception e){
            LOGGER.error(e.toString());
        }
    }

    private void getMethodReflection(Class<?> currentClass, String methodName, Object toInvoke){
        try {
            Method method = currentClass.getDeclaredMethod(methodName);
            method.setAccessible(true);
            method.invoke(toInvoke);
            method.setAccessible(false);
        }
        catch (Exception e){
            LOGGER.error(e.toString());
        }
    }

    private void consumerGenerateReflection(String type, Consumer<String> consumer){
        consumer.accept(type);
    }
}
