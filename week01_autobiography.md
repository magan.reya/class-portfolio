# CompSci Autobiography
## Reya Magan


### 1. I Am Statements
   * I am hardworking and strive to improve my skills everyday.
   * I am continuing to develop my skills and further my passion for coding.
   * I am able to persevere in difficult situations.
   * I am collaborative and work well in teams.
   * I am committed to becoming a more efficient, productive student.
   
### 2. Why Study Computer Science
Originally, I was majoring in just Electrical and Computer Engineering, but
CS201 was a required class for that major. I had very minimal coding experience
before CS201, so this was a very difficult class for me. However, the feeling of
triumph I felt by seeing the "green" APT success marks was more excitement than
I had felt in any other class before. I realized that I really enjoyed coding and
improving my skills and knowledge in computer science, and decided to take on the
double major.

### 3. Daily Programming Uses
I did two internships in which I did a decent amount of programming. I worked on
front-end development as well as end-to-end applications using cloud services like
AWS. Besides this, I don't have much programming experience in my daily life outside
of school.


### 4. Favorite Programming Project
My favorite programming project that I have worked on was in ECE350. I created a CPU
Processor and ALU, Register, Multiplication, and Division components to essentially
create a basic computer. I then used this to create a Fruit Ninja like game, and this
was overall an amazing learning experience that allowed me to combine multiple skills
to create an end product.

### 5. Programming Event (Best or Worst Experience)
I would say my worst programming experience is also my best one. I took CS310, an operating
systems class, and one of the projets that I had to complete was a from scratch creation of
a threads library. The project had a timeline of two weeks, and I worked long hours every day
but even after a week and a half I still had a 0/100 on the autograder. I felt extremely lost
so I spent time relearning threads and then attempted the project again. Slowly, I was able to raise
my score to 6, then 24, then 49, and then ultimately 70 out of 100 points. Though this was not a high
scoring project, I felt quite accomplished because I had gained a strong understanding of threads, and
was able to create a decent library in the remaining half week.


### 6. CompSci 307 Motivation
As an ECE/CS double major, I work with both hardware and software applications. In my internships, I have written
a lot of code, which though successful, may not be very professionally written. I am considering focusing on a majorly
software dependent career, and to do so I want to improve my programming skills both in a technical manner, but also write
code efficiently and clearly. I believe this class will give me strong foundation in both of these aspects. Additionally,
in my programming experiences so far, I have been more focused on code that works, rather than really thinking about the code
I am designing - through this course I believe I will design much cleaner and more readable code.

### 7. Planning and Designing Code
Since I am still not very proficient in designing good code, I feel that the initial code I will design while working on a project
will not be very clean and efficient. This code can serve as a framework for the logic of my solution, but then I will go back and
step through it to continue to clean it up and make it more professional. After practicing this for some projects, I think I will naturally
start to write more efficient code from the beginning of a project.



### 8. Advice for Next Generation
Yes, I would 100% encourage younger relatives to pursue Computer Science. In fact, after developing my passion for computer science, I suggested that my
younger brother start basic programming from middle school itself. I never wrote a single line of code until my first year of college, but soon after I learned
how versatile programming is, and how useful it is in all career prospects. My brother has already started coding basic games and web applications, so I think that
starting programming early is a great way to pave a strong career path in computer science.

### 9. Societal Role of Computer Science
With so many aspects of daily life requiring the use of technology, I think that Computer Scientists can play a pretty big role in solving societal-level problems.
I think programming combined with crowd-sourcing can combat multiple issues, one example being abuse. Creating some sort of crowd sourced application that identifies nearby help
centers and allows users to rate them can allow individuals to access instant help in cases of abuse, or when any concerns are present. This template can be used for other problems
as well, such as food shortage (connect to nearby farms and supply centers), natural disaster response (connect to shelter or nearby individuals willing to help), and many more.


### 10. Using Computer Science in the Future
I am really interested in product ideation: thinking about a consumer's needs and suggesting products that could meet that need. I think having a technical background in  both
computer science and electrical engineering will allow me to look at products from a high-level technical perspective, and my collaborative skills will allow me to understand the
needs of a user. Thus, I think this class will help me to grow my technical mind, and also continue to work well and collaborate with others. 