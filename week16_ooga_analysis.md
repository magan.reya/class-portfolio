# Journal : OOGA Project Analysis
#### Name: Reya Magan
#### Team: 3
#### Date: 12/13/2021


## Design Review

### Design Description
Our design has three major components: the frontend, the controller, and the backend.
We utilize an MVC architecture (https://courses.cs.duke.edu/fall21/compsci307d/readings/mvc-hfdp.pdf) to connect game changes on the view side to changes within the model
via the controller. In order to maintain good, separated design, we first have a MainView and MainController set up.
The MainView displays the loading screen with options to select the game to play, the player types to play the game, and 
the color/language scheme. Each selection on the MainView is associated with a Property Change Observable
event that signals to the MainController that a change has been made. The MainController then sets up a new instance of the
GameController for the new game, and the view immediately changes to the game board. 

This brings us to the second phase of our design. Here, we have a grid for the game that is currently being played, with any 
pieces (or lack of) that are needed in the initialization of the game. To visualize a specific example to step through the code, we can
imagine that we are playing the game of Gomoku. Now, Gomoku starts with a completely empty grid - as there are no intial pieces
needed to play the game. When the game of Gomoku is initialized, the GameController uses the ReflectionFactory to set up the Gomoku backend model, the player types, and
the game view in response to player types (human vs human, human vs CPU, CPU vs CPU). At this stage, the GameController will utilize the
getAllPossibleMoves() API that exists within the backend to get all of the possible moves based on the current player that is player.
For Gomoku, the every unfilled cell on the board is a valid move, so one is selected randomly. Once back in the GameController, the GameController utilizes the 
addPossibleMoves() method within the GameView. The GameView then calls the setPossibleMoves() API from the GridView which follows up by calling
the cellClicked API in the CellView that lights up all of the possible cells that a piece can be added to and creates on-click events for them. This on-click event is associated
with an observer that will promptly alert the GameController that a move has been made on the view side. These described interactions happen at every iteration of the game, as after each 
move a new set of possible moves are generated, so all of these APIs are utilized.

In response to placing a piece on a cell with the on-click functionality, an observer is sent out the GameController to signal this move.
The GameController then calls its step function - which utilizes multiple Model APIs to play the logic of the game. It first checks to see if the game
has already been won using the getGameStatus() API. If the game has not ended, then the Move() API is called with the point that the current player just tried
to place a piece at. The model validates this, and if the move is a valid move it goes through the logic to update the game Grid, and also uses the checkEndGameCondition() API to check if
the current move causes the game to end. The GameController then calls the incrementScore() API to get the current score of the game, and this is sent back to the view. The GameController also uses the ImmutableGrid()
API to send the current states of the game board to the view to display as we do not want the view to be able to change the Grid - so it utilizes an Immutable Grid interface to 
keep track of the current states of the grid. These API calls also happen for every turn of the game, until checkEndGameCondition() returns true. Once it returns true, the game is over, and GameController 
will signal to the view to display the winning player using the showWin() API.

This is an example of how the APIs within our MVC system interact with each other. There are few more APIs that are used in the CPU specific games - these can all be found in the Algorithms API. We basically 
have a defaulted recursive MiniMax algorithm that continuously chooses the best move for the CPU player. The rest of the APIs works the same as described above. As shown here, our design is very well connected through
Property Change Events, APIs, and Factories. The controller maintains these connections, and the view constantly sends observer events to the controller to signal the model to make changes. We have a very robust design that is closed to modification
and open for extension using our existing APIs.


### Change Scenarios

* Adding new game variation

If we wanted to add a new game to our design, it would be very easy to do so. Let us first determine that the logic of the game requires a user to put X number of pieces in a row X number of ways.
That means that this game requires some sort of connection logic. So we can look at how each of the classes would need to change in order to make this change:

Model: First we can look at this class to see how it would be in the model. When we go into our model package, we see that we have an abstract model class, and an abstract connection model class that extends this model class.
This abstraction is perfect for our new game. We can simply create an extended Class called "YModel" for this new game, Y. However, this game seems to be a little different in logic in comparison to the two other games extending this
abstract Connect Model. Those games (Gomoku and Connect Four) require some number of pieces in a row once, whereas our game requires X number of pieces in a row X number of times.
So, we can use polymorphism to override the hasInARow() method within the ConnectModel class to add this new functionality. Then we are done in terms of the model. Because we have deep abstractions within our inheritance hiearchy that allow us
to utilize polymorphism effectively, we can easily add new games depending on the category they belong to. This design also follows Single-Responsibility principle and Dependency-Inversion because instead of having the common methods from our ConnectModel be in 
the general abstract class, we have them in the ConnectModel class so that the general Model does not have to depend on the Gomoku, Connect Four, and Y game subclasses to use certain methods. It also thus encapsulates the necessary methods to the ConnectModel class.
After we have the model finished, we do not really need to add anything for our CPU algorithm, as the MiniMax algorithm would work in the same way as the other connection games. For a more complex game (like Checkers or Othello) we would need to make relevant changes to the
algorithm, and we could just override the recursiveMiniMax API to do so. Our model also follows Liskov-Subsitution as the general Model has very little functionality, if we replaced it with a game like Gomoku it would not break anything (though other games would probably not have correct functionality at
times) (https://www.digitalocean.com/community/conceptual_articles/s-o-l-i-d-the-first-five-principles-of-object-oriented-design,
https://medium.com/@benjaminpjacobs/the-four-principle-of-object-oriented-programming-f78600f62608).

Data Files: After we set up the model, the next thing we need to do is set up the data files for the default version of this game. This would be fairly simple, as we would need to create a sim file with Author, Default Player type, and Default csv board files. We would then create 
a default CSV board of the same name as the sim file and fill it with the initial conditions of the game. We have a SIM and CSV parser class that would not need to be changed at all: they follow the open-closed principle as they will parse any valid file that is passed. We also have a packets class
that would combine all the relevant sim and csv data and the GameController would get this information at initiation so that it could
display a default board if no file was loaded. 

View: In terms of the view, we would just need to add this game Y as an option for games to play, we would update the Config file to have this so that this game could be initiated via reflection. Because we already have a robust system of property change events within the frontend, we would just need to make sure that the selection of 
this new game is associated with a property change event - this automatically holds true as we already have game selection associated with property changes.

After these minimal changes, the game would work perfectly as intended.

* Changing data file format

If we wanted to add a new data file type to create the initial game states, we would not need to change many classes, but we 
would need to add these classes. As we already have a parser interface, to add JSON or XML considerations, we would just create parser classes
for these two data types and have them implement the parser interface. This would follow the interface segregation principle and allow us to add new
functionality for parsing. This would be similar for the packets class, except in this case we have an abstract packets class. We would make JSON packet and XML packet classes
and have them extend the abstract packet class and override methods as necessary to satisfy polymorphism.

The only class we would need to change is the GameController, which currently uses the SimPacket and CSVPacket classes. What we could do is create a method that uses reflection to read the ".X" ending of an inputted file and initialize
the correct packet class as a result. This is not currently implemented in the code, but could be done easily and would satisfy single responsibility by giving the packet choosing responsibility to a factory class.

Overall, it would be quite simple to add new data file types to our program and we would need to make only minimal changes.

* Add new Player view

This is something that is truthfully a little difficult to implement in our design, but it can be done. One thing that we were planning to implement was a running list
of the high scores for a player. If we were to implement this it would work somewhat as follows:

First, we already have the mechanisms for scoring implemented within our game using the incrementScore() API within the model to consistently update the score for each player and display it on the view.
When the game ends, we have these scores stored in our backend as well as the frontend. Now, we already have a property change event called gameViewSaveClicked/gameControllerSaveClicked that sends an observer once
save game is clicked. What we can do is have another observer attached to the win screen method. When the win screen method is called, we can take the winner's name and their score, write it to an existing file, and send this file to the controller to be saved.
Then, in terms of data files, we are maintaining a running list of high scores.

To actually display this we would need to edit our MainView class to have a VBox that displays a JavaFX text display element that shows the latest X scores for previous runs of all the games. We would need to add relevent JavaFX elements to the ViewElements class to do this,
and also rearrange the already existing structure in the MainView. This would be quite a few changes, but it would not break any code.

Thus, adding a new Player view is not completely possible in our design as is, but we do have our property change events that follow single responsibility (their task is just to notify) that can be utilized to set this up.

### Design Pattern Example
One design pattern that was implemented was the factory design pattern. Our program relies on a lot of reflection to run as intended, and over-time our code became very dense with the amount of reflection set up and initialization that needed to be done.
To improve this, a ReflectionFactory class was created along with a ReflectionSetup class. Examples of them are below:
```java
//Reflection Set Up
public Model setUpModel(String gameType, int[][] states) {
    Class[] modelConstructorClasses = {int[][].class};
    Object[] modelConstructorValues = {states};
    Model model = (Model) getClassReflection(
        Config.DEFAULT_MODEL_REFLECTION_PATH,
        modelConstructorClasses,
        modelConstructorValues,
        gameType);
    return model;
```
```java
//Reflection Factory
public Object getClassReflection(String config, Class[] classParams, Object[] classValues, String type)  {
        try {
            return Class
                    .forName(String.format(config, type))
                    .getConstructor(classParams)
                    .newInstance(classValues);
        }
        catch (Exception e){
            LOGGER.error(e.toString());
        }
    return null;
    }
```
Above are two methods that can be used when reflection is needed. If we want to instantiate a new model class, we
simply need to first pass the relevant parameters to the setUpModel() function in the ReflectionSetUp class. Then, we can
call the getClassReflection() method within the ReflectionFactory to actually do the reflection.
All of this data is completely encapsulated from the rest of the program. A class will simply request the Reflection package to set up and execute 
reflection, and the factory will promptly respond when given the necessary parameters. We can then refer to the new object using the result from the reflection class.
With this factory we cut down on a lot of code, and also maintain strong encapsulation throughout the code (https://www.oodesign.com/).
### API Examples

#### Good

* Describe as Service

An API that stayed consistent throughout the project was the ImmutableGrid API. A few of the public methods can be seen here:
```java
/**
   * Determine the state of a Grid's Cell at a given location
   *
   * @param col - the Cell's column (y)
   * @param row - the Cell's row (x)
   * @return the value of the Cell's state (usually PlayerID or 0 if uninitialized)
   */
  int getState(int col, int row);

  /**
   * Get all "pieces" within the Grid
   *
   * @return - Set of "pieces"
   */
  Set<Point> getAllPieces();
```
These are just two of the APIs available within the Grid class. These exist as part of an ImmutableGrid that can be accessed throughout the code to check current 
states of game pieces.

* Easy to use and hard to misuse

This API is extremely easy to use - it only exists for data purposes. As we are dynamically changing our Grid in the backend, 
this interface keeps track of these changes in real-time. If at any point any class needs to know the current state of the grid, it
can call any of the methods within the ImmutableGrid API to do so.

There is no way to misuse this API as it does not allow for any changes to be made to the Grid. It solely exists as a data class.

* Encapsulation and extension

This API is a strong example of encapsulation and extension. First, the view uses an instance of the ImmutableGrid object as its Grid.
What this means is that the view has no control over the Grid at all, which is exactly what we want. The View cannot make any direct changes to the Grid,
it must send a PropertyChangeEvent to request a change. It does however, know the states of the Grid at all times because the ImmutableGrid is always updated
to reflect the real grid on the backend.

If ever we wanted to add more features to a grid such as getPreviousGrid() or getDiagonalPieces() we would just need to add these methods to
out ImmutableGrid API. We already have functionality that we can return different types of pieces within a grid, so we would just need to add another method to access certain
keys within our Grid and just return those. It is extremely simple to both use and extend this API, and the API will never mess up any aspect of the game.


#### Needs Improvement

* Describe as Service

The API that needs some improvement is the MiniMaxAlgorithms API. Currently, the model implements the MiniMaxAlgorithms Interface to run
all of the necessary methods for the CPU version of our 4 games. In some of the games, the algorithm makes a possible move to score it, but it then
needs to clear this move and any changes made by it. This is the API involved with that:
```java
/***
     * This method undos the previous execution resulted from running the minimax algorithm.
     *
     * @param point is the location where a piece was placed before only for running the minimax algorithm.
     */
    void recoverFromMiniMax(Point point);
```
Here is the method that actually makes possible moves:
```java
/***
     * This method will place a piece at the location passed in, simulating the real interaction of a game.
     *
     * @param point is the location that minimax algorithm wants the player to place a piece at,
     *              so that it can record the gains and losses.
     * @param playerID is the player's ID.
     */
    void tryMiniMax(Point point, int playerID);

```
These APIs are called in a few of the extended model classes as some of them require polymorphism to override these methods. This is fine, but the problem is these methods can theoretically be called
from anywhere and can change the code. 

* Easy to use and hard to misuse

The MiniMaxAlgorithm API is definitely easy to use - all that needs to be done is that the recursive algorithm must be called. However, the methods tryMiniMax() and recoverFromMiniMax() directly affect the board. If the view used this method, it would alter the game completely.
Therefore, this API is not hard to misuse, as one wrong step can impact the rest of the game.

* Encapsulation and extension

This API was created with the purpose of encapsulation in mind as we wanted to keep the CPU algorithms away from the actual Model general class. This 
satisfies that, but in my opinion causes more issues than it fixes. It is extendable as we can always add new CPU Algorithms as interfaces (such
 as a "dumb" algorithm that randomly predicts a move). All this being said, I believe that this interface should have existed as a class for
reasons I will describe in a later section.

### Code Design Examples

#### Good Example **teammate** implemented

* Design

Parser Interface and Implementations

* Evaluation

One of my teammates implemented a parser interface that deals with the parsing of the sim files and csv files,
and I felt that it was really modular and extendable. An example is below:
```java
// Function to load file given a string filepath
//METHOD IN PARSER INTERFACE
  void loadFile(Packet packet) throws Exception;

          
//OVERRIDE IN CSVPARSER
/**
 * Given a Packet object, we obtain the file path of the SIMPacket file. We then scan the CSV file
 * line by line
 *
 * @param packet is a Packet object passed from the SIMParser
 * @throws Exception throws a ParserException to the MainView
 */
@Override
public void loadFile(Packet packet) throws Exception {
        SIMPacket simPacket = (SIMPacket) packet;
        String filePath = simPacket.getInitialState();

        CSVPacket csvPacket = new CSVPacket(simPacket.getInitialState());

        checkFileType(filePath, Config.CSVFILE_TYPE, Config.INVALID_FILE);

        File file = new File(filePath);
        Scanner in = new Scanner(file);

        // Parse and check the first line
        getParserKey(Config.CSVPARSER_NUM_ROW_COL).evaluateLoad(in.nextLine(), csvPacket);
        csvPacket.setUpStatesArray();

        // Parse the rest of the file
        while (in.hasNext()) {
        csvPacket.checkOutOfBounds();
        getParserKey(Config.CSVPARSER_GRID_STATES).evaluateLoad(in.nextLine(), csvPacket);
        }
        // Do final check
        csvPacket.checkCorrectRowDimensions();
        csvPacket.resetCurrRow();

        // Set csv packet
        setCsvPacket(csvPacket);
        }

```
I think the Parser interface is a strong example of the interface segregation principle (https://courses.cs.duke.edu/fall21/compsci307d/readings/isp.pdf), because it
splits up common methods into an interface and then overrides them through polymorphism. In my previous project (Cell Society) something I struggled with was figuring out a way to
have my SIM and CSV parsers extend from some sort of abstraction, because the two classes were so different.
I think this was a smart way to do this as the Parser interface consisted of general methods that
were common to both parsers (saveFile() and loadFile()), but each extended class implements these methods in its own
way and also has its own methods. I think this is a good example of polymorphism thus, as we are able to visualize these
classes as an extension of the interface, but they also have their own independent functionality. I think this is extendable for any
future parsers someone may want to add as well, as they will all have some similar functionality like saving/loading files. They would just need to create
a new parser class that implemented the parser interface and its methods in the way needed for specific file type, and they would also add any
other methods needed to parse the new file type.

#### Needs Improvement Example **teammate** implemented

* Design

Organization of property change event methods.

* Evaluation

One thing that was used extensively to connect the frontend to the controller were property event changes.
I think this was a really strong idea, as it followed single responsibility principle and also made sure that the view
never called the controller directly - which is something my previous teams have struggled with. I was really impressed by
this idea, but I feel that the code could have been designed a little differently. Below is an example of how an example property
change was used within the GameView class:
```java
/**
   * Given a Player, show a won game dialog for them if they have won.
   *
   * @param winPlayer
   */
  public void showWin(Player winPlayer) {
    isWin = true;
    String winMessage = String.format(myResources.getString(Config.WIN_MESSAGE),
        winPlayer.getName());
    Alert alert = viewElements.createAlertDialog(winMessage, winMessage, AlertType.CONFIRMATION);
    ImageView checkImage = new ImageView(Config.CHECK_IMAGE);
    checkImage.setFitHeight(Config.CHECK_IMAGE_DIM);
    checkImage.setPreserveRatio(true);
    alert.getDialogPane().setGraphic(checkImage);
    alert.show();
  }
/*************************** Everything below is Observable/Observer pipeline **************/
@Override
public void propertyChange(PropertyChangeEvent evt) {
        Class<?> currCLass = GameView.class;
    reflection.getNonClassReflection(Config.REFLECTION_FACTORY_PROPERTY_REFLECTION, currCLass,
            evt.getPropertyName(), this, evt);
            }

/**
 * Add piece pipeline Helper function is required for different gametypes (Human vs. Human) (CPU
 * vs. CPU) (Human vs. CPU)
 */
private void gameViewAddPiece(PropertyChangeEvent evt) {
        gameViewAddPieceHelper(gameViewTopPanel.getCurrPlayer(), (Point) evt.getNewValue());
        }

private void gameViewClickPiece(PropertyChangeEvent evt) {
        notifyObserver(Config.GAME_CONTROLLER_CLICK_PIECE, evt.getNewValue());
        }
```
As seen in the code, there is first a method relevant to the GameView class itself - showWin() shows the winner of the current game when it ends.
Then, there is a break in the code signalling that everything that follows is related to the Observable/Observer pipeline. This is a really intelligent decision,
but I think the way it is organized within the GameView file makes the code less clean and harder to follow. I personally needed some time to map out all of the observable pathways.
I noticed that a lot of these methods that exist under the division of GameView code and PipeLine code are helper methods. I think we could have perhaps made a separate
property change package that would have pathways organized for each of the classes that use them. We would simply need to instantiate it from the class that calls it. Already the method names exist within
the Config.java file, so we would need to update file pathways. I will admit that I do not have as much expertise on Observables, so my theory could be incorrect, but still I think a lot of the helper methods
could be extracted into a sort of PropertyChangeHelperFactory class. In my opinion this would help the GameView class maintain single responsibility of keeping the game running, and would also alleviate some
code smells. That being said, I think this was a wonderful idea that greatly added to our project as a whole.


## Your Design

### Design Challenges

#### Design #1
Model instance vs Player
* Trade-offs

A major design challenge that I and the backend team as a whole struggled
with was how the Player and Model classes should interact. At first we saw the program as the player making a move -
meaning that we would utilize the player class to prompt logic in the model. This intuitively made sense, but a major
tradeoff was that it made out model class dependent on the player class - when in fact the model (the logic hub) was 
was a more important class.
* Alternate designs

Instead of having the player have an instance of the model, we considered flipping this so that the model would have an list of the current players
within the game passed as an instance parameter. This however seemed quite confusing - if we passed players within to the model and then checked the player and made
the move within the model - what exactly would be the role of the player? We could just have all of this functionality within the model itself. However, if we
completely got rid of the players class, the model would have too much responsibility. So this idea was also quickly scrapped.

* Solution

Finally, as a team we were able to decide that the GameController would have instantiate both the Player and Model classes. This way, the controller would set up the list of current players (combinations of human and CPU), and also the current game.
The model would be able to access the player class as it is above it, but the player class would hold name and type information, which the model class would have to request.

* Justification or Suggestion

I think this is a good solution because it separates out the player and models still for clean code and single responsibility (and fixes our original dependency issue). However,
it also does not make the player class redundant. The player class has important information such as the name and score of the current player - this is useful for the score board and if
we ever wanted to add a high score feature. Overall, this spreads out our design and also allows for extension to add new features.


#### Design #2
Invoking private methods within the ReflectionFactory
* Trade-offs

I created a factory class called ReflectionFactory to deal with the different types
of reflections throughout the game. The ReflectionFactory class has two public methods:
getClassReflection() and getNonClassReflection(). When reflection is needed the relevant method is called with
corresponding parameters. This sets up the reflection but when it came to invoking method or field reflection, it was often happening
on private methods within a class, so this was still leaving long lines of reflection code within other classes.

* Alternate designs and Solution

An alternate design that I found was the setAccessible() method to temporarily set a private method as accessible. This
was something I was very hesitant to do at first - as I was not sure if it was a good idea to make a private method 
accessible to a factory. However, after discussing this idea with my UTA and making sure this quick accessibility change could not make the method
easily called from another class, I went forward with it. It worked out well, and the factory class can now initiate and invoke reflection at request.


* Justification or Suggestion

I think this is a valid solution, because it takes away a lot of responsibility from independent classes and separated
it out into factories. I was concerned about the possible access issue - would another class be able to access a method that should be private
but was temporarily made public? This however is not the case, as the factory makes the method temporarily accessible,invokes it, and then makes 
it inaccessible again. This way we can maintain its private functionality but also invoke it using our factory.


### Abstraction Examples

#### Good

* Classes/Interfaces included in abstraction

I created the ConnectModel abstraction from the general Model class.
The ConnectModel extends from the Model class and two games - Gomoku and Connect 4 extend from this ConnectModel
We can see this below:
```java
public abstract class Model implements ModelMovement, MiniMaxAlgorithm {}

public abstract class ConnectModel extends Model {}

public class GomokuModel extends ConnectModel {}
```
This shows our multi-level abstraction. The Gomoku game is a specific model that implements
everything within Model, ModelMovement, MiniMaxAlgorithm, and ConnectModel, because it is extending
and implementing all of these.

* Design goal

The design goal with this was to fix dependency inversion within our model class. Prior to this change, there were specific
methods for Gomoku and Connect Four placed into the abstract model class because having them in both games would duplicate code (because
the two games were so similar). However, this then made the model dependent on Gomoku and Connect Four subclasses as these methods 
were only used if one of those games were being played. The high level abstraction should not need to rely on any low-level specifics, so I
knew this was something that needed to be fixed.


* How it helps

This separation fixes the dependency inversion issue as now the model does not depend on Gomoku or Connect Four at all. This was
a fairly simple fix - we did not need to change anything in any other package: the Gomoku and Connect Four games can be created easily
as an instance of the general Model while still extending ConnectModel. This also allows for more extension, as we can easily add new connect type games, or could even make\
more abstractions for other similar games should they be added.

#### Needs Improvement

* Classes/Interfaces included in abstraction

As described above, we currently have a MiniMaxAlgorithm that is implemented in the model for the CPU methods. 
However, I believe that some of these methods should not be public, and that the Algorithm should exist as an extended abstract
class from some "GameDriver". The two methods in concern are re-iterated below:
```java
/***
     * This method undos the previous execution resulted from running the minimax algorithm.
     *
     * @param point is the location where a piece was placed before only for running the minimax algorithm.
     */
    void recoverFromMiniMax(Point point);
```
```java
/***
     * This method will place a piece at the location passed in, simulating the real interaction of a game.
     *
     * @param point is the location that minimax algorithm wants the player to place a piece at,
     *              so that it can record the gains and losses.
     * @param playerID is the player's ID.
     */
    void tryMiniMax(Point point, int playerID);
```

* Design goal

The goal with this change would be to encapsulate some of the functionality of the CPU algorithm more. Right now it can be accessed quite easily,
just one line could cause the view to make large changes within the game board. In order to keep the current abstractions that the algorithm provides while making
some methods more private, we could make it an abstract class and have different GameCPUs extend it.

* How it could be improved to help more
  
Personally, I do not think that this interface should exist. I think it would have
  been better to have a completely different structure for the CPU. I had ideas of having a sort of abstract "GameDriver" class that would be extended by an Algorithms class and a HumanModel class which
  the different games would also extend. This would create a lot of abstractions in our code, and I feel that we would be able to only have the necessary APIs as a result (such as the recursiveMiniMax() algorithm
  itself). The above two methods could likely be private in this different design. All the common methods between both the human and CPU algorithms would be in the driver class, and independent functionality would exist in the extended classes.
We would need to make sure that if there is some method that the CPU needs that is overridden in an extended game class would be called correctly, but this would just need
some creative uses of reflection to implement.


### Code Design Examples

#### Good Example **you** implemented

* Design

Two good designs that I implemented are already described above (ConnectModel and ReflectionFactory). Another design example
that I feel was important (even though it did not affect functionality) was the organization of all of the APIs within our program. For example, many of the
Model APIS can be seen in the ModelMovement API class. An example is below:
```java
/**
     * This method will be called by the game controller to do the movement (placement) of pieces in the game.
     * It takes in the point to play and the player playing it, and throws an exception in the case
     * of an invalid move
     * @param played: a point object at which a piece is to be placed
     * @param p: the player object placing the piece
     * @throws Exception: throws a custom "Invalid Move" exception if an invalid position is selected
     */
    void move(Point played, Player p) throws  Exception;

    /**
     * A setter to set the position to move a new point to. It calls a method to check if the placement
     * is valid, and returns a boolean as to whether or not the player can make the proposed move.
     * It takes in the column and row for the point to be placed at. For example, for Othello one can
     * only palce a piece if doing so will surround pieces of opposite state and flip them. If this is not the
     * case, this method will return false.
     * @param col: column of point to place
     * @param row: row of point to place
     * @return boolean as to whether the placement can happen
     */
    boolean setPositionToBePlaced(int col, int row);

```
This design was not added for functionality purposes. It was added because at one point I was looking through the code and felt that it would be hard for some third-person individual to use it or figure out the APIs that they could leverage. I decided to separate out all of the APIs for each package into API packages within them, as this ultimately allowed for better organization and clarity. It even helped me to gain a better understanding of the program as a whole.
* Evaluation

I think this is an important design addition because even though it does not add explicit functionality (I just made these APIs interfaces to be implemented by respective classes), I think it makes the code much cleaner and easier to understand.
I think if someone wanted to leverage our APIs now, it would be simple because they could just access specific packages and use what they need. Earlier, they would have had to search for public methods throughout the code. This was a great idea that I discussed in lab with 
a classmate, and I feel that it has made our overall design and API interactions much easier to understand and visualize.

#### Needs Improvement Example **you** implemented

* Design

Certain logic within the game of Othello

* Evaluation

I developed the entirety of the game of Othello (both Human and CPU) for the backend. Othello
has fairly complex logic, so it took me some time to learn the game. Because of this, though I tried to maintain
my design as I developed functionality, some methods became quite dense. An example is below:
```java
private void findFlankCells(Cell flankCell, int directionX, int directionY){
        cellsToFlank.clear();
        cellsToFlank.add(flankCell);
        while(true){
            Cell pieceToFlank = getGrid().getCell(setUpPoint(flankCell.getLocation().x + directionX,
                    flankCell.getLocation().y + directionY));
            if(pieceToFlank != null && !pieceToFlank.isEmpty()){
                if(pieceToFlank.getState() == flankCell.getState()){
                    cellsToFlank.add(pieceToFlank);
                    flankCell = pieceToFlank;
                }
                else{
                    canFlank = true;
                    return;
                }
            }
            else{
                cellsToFlank.clear();
                return;
            }
        }
    }
```
This method is used to find cells to flip once a piece is placed on the board. It keeps a running list
of the cells that need to be flipped - the criteria of a piece to be filled is that it must be different than the piece
that was just played and not be null or empty (so it needs to be the opposite player). The current logic continues to add these pieces to the cellsToFlank until
a piece with the current player ID is found (we want to surround the opponent's pieces to flip them). Once this happens, we set a boolean that a valid flank is found and immediately return. If
no such condition is found we just clear our list and return. This is very conditional logic, so I ended up have a lot of nested loops as can be seen. I really wanted to try to use consumers or streams to fix this, and
I spent a long time trying out various methods to reduce this. Unfortunately, I really struggled with this specific method and was not able to figure out how to effectively refactor it to use either consumers or streams. I ended up leaving it as is for the sake
of functionality. I'm not very happy with this, but at the same time still am unsure how to fix it. I guess this is a tradeoff that sometimes needs to be made in the case
of very specific logic. This is something that I would like to come back to, though.



## Conclusions

#### What part(s) of the code did you spend the most time on?
I spent definitely the most time actually implementing the logic for the game 
of Othello for this project. I had never played the game, so it was really challenging
for me to implement not only the full functionality, but also good design for the game.
Because it was such a new game for me, I mapped out a lot of possible scenarios on paper before
even touching the code - this took at least 2 hours. It was tedious at times, but I feel that I was
able to implement strong functionality of the Othello game while also maintaining well designed code.

#### What is your biggest strength as a coder/designer?
I actively think about design as I am coding. This was not something I ever did before this class, but now as I am coding,
I feel like there is a whole other world in the back of my mind trying to find ways to make my code better. I still have ways
to improve in how I design my code at times, but I have really seen a change in myself as to how I approach coding and desiging
programs. I am someone who always strives to do better, and so I believe I will continue to grow my design skills throughout my 
career as a software engineer.

#### Thing #1 you have done this semester to improve as a programmer/designer
The first thing that I have done this semester to improve as a programmer/designer is to think about design AS I am writing
functionality. I remember the first few projects of the course I spent so much time making the games work that I did not actually 
implement well-designed code. Granted, this was the beginning of the semester, but coming into the semester I had already developed the 
habit (from my previous CS courses) of coding to get things to work. It took a lot of practice for me to put design before functionality (truthfully
at times I catch myself getting caught up in functionality still sometimes), but I have greatly improved and left this habit behind for the most part.
When I hit a wall in terms of functionality, I start to refactor my code more. Often, this actually illuminates the issue that exists in my functionality
and I am able to have good code and good functionality at the same time! This new habit that I am developing of refactoring and designing good code throughout
the programming process has really improved my skills and confidence as a programmer and will continue to do so for years to come.

#### Thing #2 you have done this semester to improve as a programmer/designer
Another thing that I have done this semester to improve as a programmer is to have more confidence in myself to speak up when I have ideas. In CS teams I often feel that everyone
else in the team has more experience and as a result probably have better ideas than me. I have come to learn that a lot of this is in my head. Of course there are times where my ideas
are maybe not the best ones in the context of the problem, but throughout these team projects I have learned that I can provide valuable design choices to the code as well. Little by little,
I have been working to improve my confidence in coding and push myself to take on difficult design challenges and have discussions with the team about what constitutes good design and what code
sometimes needs to be refactored. There are times where I still feel a little closed off, but I can confidently say I have changed a lot from the person I was when I first
began this course. Growing my confidence can't just happen overnight, but I feel that through this course and through my efforts to speak up when I have ideas, I become a little more 
confident every day.

#### What is your favorite part of working on "large" team software projects?
It is so exciting to see an application come together from start to finish. I personally worked on the backends for the majority of the projects (I was on the frontend for OOLALA), and it is fascinating
when people with different backgrounds and skill sets come together to combine ideas and designs for not only really fun applications, but really well thought out designs. I never knew that well designed code 
could give me so much excitement, but I have so many great memories working with large teams in this class and coming together to write really good code and feeling so satisfied. It is a different feeling come to 
a solution with a team than it is on your own. I am really thankful for this experience, and am looking forward to working in large team software projects in the future.