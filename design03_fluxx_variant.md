# Fluxx Objects Design
## Reya Magan


### Tracking Changes

#### Changes needed in order to support Zombie Fluxx version
* new classes: Creeper, Ungoal



#### How new design reduces future logic changes

I noticed that the Creeper and Ungoal cards are very similar to the Keeper and Goal cards, respectively. Basically, they both work like their respective counterparts - but just have an opposite outcome. For example, a goal shows what cards are needed to win, whereas an ungoal will show what cards cause the zombies to win instead. Since these share somewhat similar logic, I think Creepers and Ungoal classes could be inherited classes from the Keeper and Goal parent classes. This would allow for more cards such as these two to be added with minimal logic issues.


### CRC Card Classes (Updated)



1. This class's purpose or value is to represent a Creeper:

|Creeper| |
   |---|---|
|**Public Void Read_Creeper** *parameters* = (HashMap) Creeper_Card |Game_Setup|
|**Change_Creeper Method:** *parameters* = (HashMap) cards_in_hand; *return* = (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile | Action |
|**Check_Creeper Method:** *parameters* = (HashMap) cards_in_hand, (Pair) Current_Goal); *return* = (Boolean) Game_Is_Won |Rules|
|Public Variable (Pair) Current_Goal |Goals, Ungoals|
|Public Boolean Game_Is_Won |Keeper|

```java
public class Keeper{
  //When a Creeper card is drawn/used, this class deals with the possible outcomes
  public void Read_Creeper(HashMap<Integer, String> Creeper_Card){
    //This is the method that another class will call, this method will then call the method below to change the Creeper.
  }
  public ArrayList<HashMap<Integer, String>> Change_Creeper (HashMap<Integer, String> cards_in_hand) {
    // This method will replace the current creeper
  }
  public Boolean Check_Creeper(HashMap<Integer, String> cards_in_hand, Pair<Integer, String>Current_Goal){
    //This method checks to see if Creeper cards match the goal, if yes then it returns true and the game ends
  }
}
```
2. This class's purpose is to represent an UnGoal

   |UnGoals| |
   |---|---|
   |**Public Void Read_UnGoal** *parameters* = (HashMap) UnGoal_Card |Game_Setup|
   |**Change_UnGoal Method:** *parameters* = (Pair) Current_UnGoal; *return* = Pair (Current_Goal) |Action|
   |Public Variable (Pair) Current_UnGoal |Rules|
   | |Keeper|
   | |Creeper |
   | | Goals |
```java
public class UnGoals{
    //When a UnGoal card is drawn/used, this class deals with the possible outcomes
   public void Read_UnGoal(HashMap<Integer, String> UnGoal_Card){
       //This is the method that another class will call, this method will then call the method below to change the UnGoal.
   }
   public Pair<Integer, String> Change_UnGoal (Pair<Integer, String> Current_UnGoal) {
      // This method will replace the current ungoal
   }
}
```

### Game Loop (Updated)

* After setting up the game, turns proceed like this:
    1. Draw the number of cards currently required (start with Draw 1)
    2. Play the number of cards currently required (start with Play 1)
    3. Discard to comply with any Limit rules in play (start with no limit)
    4. The game ends when a player meets the conditions of a current Goal card.
    5. If a player draws a Creeper card, they must place it front of them and complete any instructions immediately
    6. If a player draws an UnGoal card, this can overwrite the Goal card essentially such that if any player meets an UnGoal condition the zombies win.


### Use Cases (Updated)

* A player plays a Goal card, changing the current goal, and wins the game.
  * This means that the player will go into the "Goal" class, and replace the current goal, this will then call the Keeper class, and after looking through the player's current Keeper cards, a match would be found between them and the new Goal card -> causing them to win.


* A player plays a Rule card, adding to the current rules to set a hand-size limit, requiring all players to immediately drop cards from their hands if necessary.
  * This Rule change will recognize that a hand-size limit means that it needs to use its Change_Cards method - It will go through each player and make them remove the necessary number of cards before resuming play.


* A player plays a Rule card, changing the current rule from Play 1 to Play All, requiring the player to play more cards this turn.
  * This Rule change will also call the Change_Cards method, to prompt the player to play all of their cards. If a specific card is played here - say, an action card - then that class and its relevant methods will also be called.


* A player plays an Action card, allowing him to choose cards from the discard pile and play them.
  * This will call the Action class, specifically the Change_Cards method. The player will gain more cards, and then play them. Whatever card they play (for example a Keeper card), the respective class and its methods will be called to process it.
  

* A player plays an Action card, allowing him to choose cards from another player's hand and play them.
  * This will call the Rotate_Players method in the Action class to take those cards from another player and then play them. Whatever cards are played, those classes and methods will also be called and processed. 

### Use Cases (New)

* Zombie card 1 - Brain Baseball Goal
  * If you have already drawn certain Keepers or Creepers (Baseball bat and Zombie), and happen to also have a Baseball keeper, if this Goal card is drawn you would automatically win as you meet the requirement of 1 baseball bat, a brain, and a zombie. This would use the Check_Keeper and Check_Creeper methods in their respective classes, and compare them to the Current_Goal variable

* Zombie card 2 - Zombie Victory UnGoal
  * If there are 4 zombies on the table, and one player plays another zombie, if someone has this card the zombies will automatically win. This would utilize the Check_Creeper method from the Creeper class as well as the Current_UnGoal variable to find whether they are equal - if so, the GameIsWon variable will return true for zombies.

* Zombie card 3 - Larry Creeper
  * If this card is drawn, then the player cannot win no matter what the Goal says. This would be part of the Check_Creeper method in the Creeper class. If it is true that a player has this card, even if the current goal (Read_Goal from Goals class) allows for a win with zombies, this will be overwritten. An action card that causes Creepers to be changed (Action class, Change_Creepers method) would need to be used to officially get rid of this card and be able to win again.

* Zombie card 4 - Return of the Dead Action
  * All of the Creepers that have been discarded will be drawn in the Action class (Change_Creepers method) and be dealt to players using the Rotate_Players method. The players will then place these Creeper cards out on the table and if at this point a goal or ungoal is met the game would end - otherwise it continues on.
