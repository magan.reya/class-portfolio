# Journal : Starting
#### NAME: Reya Magan
#### DATE: 9/27/2021


## Starting a Big Project

### First design challenge
The first design challenge that the team chose to work on was the implementation of the Logo Programming game. The Logo game
seems to be the most basic of the three, and both L-System and Darwin draw from it to some extent. We thought it would work well
to start with a specific, simple game such as Logo Programming, and set up multiple use cases for that. This way, we would have a strong
design that is functional, and can also be extended to the other more complicated games once we get to that part.

### First piece of code
The first piece of code that we wrote was everything needed for a basic "fd 50" use case. We set up a visual <- controller -> model
organization to control commands as they come in and then process them and display them on the screen. When doing this use case, we were
able to set up necessary command classes as well as a strong model and visual component and once we got this to work, we knew it would be
extendable to more commands as well.

### Hardest part or biggest obstacle
The hardest part was getting started. The first three meetings with the team were purely ideation and design set up. It took a really long
time to work through all of the design and organize the components in a modular and clean manner. We had to map out multiple
organization structures and go through multiple use cases before reaching our final design. This was a very long process, but I felt that the team had
a good idea on how to move forward after we had crossed this design obstacle.

### First coding session insights
We spent about 2 hours in person writing code, and then 3 hours on our own. I feel like we accomplished a lot, as the Logo Programming has a lot of functionality. Having a design laid out was 
extremely helpful, and it didn't feel super difficult to start writing code. I think it would be nice to get some more commands added so Logo Programming can be fully functional.

### Successful outcome
We want a dynamic game that allows users to play any of the three applications with full functionality and ease of use.
This is off to a good start, as Logo Programming is going well and L-System and Darwin can extend from this.

### Worst possible outcome
We think we can get the applications to all work well, but if it comes down to some functionality being
difficult to develop, we would have to sacrifice that to keep a good design still. However, so far our design seems
to be extendable to full functionality.


## Ethics Essay

### Ethical questions regarding AI/Robots

* What happens when AI/robots replace humans doing a specific job (such as factory workers, truck drivers, or radiologists)?

As AI/robotic technology continues to develop to the point that these robots can carry out human tasks, people have began to notice
the various jobs that can be done with these robots. A lot of industries, especially manufacturing, have factory workers at their assembly
lines, and these people are generally paid around minimum wage. With these new robotic technologies, these industries are looking to replace
minimum wage workers with these robots, as it will save them money since they will not need to pay the robots. While this can make sense when looking
at a company's finances, and it would definitely save them money, if these jobs become obsolete it would increase unemployment rates. Some Whole Foods stores
have already implemented a mechanism where your cart is scanned before you leave - so no cashiers are employed. With this new technology, it does make sense why
a company would want to save their costs by using it, but I think that something should be set up to help the factory employees, cashiers, etc to find new jobs. I think some portion
of the costs that are "saved" by employing these robots should be put toward development programs sponsored by the company to help unemployed people get back into the workforce
and join different roles at these companies. I think technological advancement should not come at the cost of livelihood for humans, rather they should grow together.