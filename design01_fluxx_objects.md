# Fluxx Objects Design
## Reya Magan


### High Level Design Ideas
In order to design the Fluxx game, I felt it would be good to create
a separate class for each card type (Action, Keeper, Goal, Rules), as well as a central class that will initialize the game and keep track of it as play continues. In my design, I decided to use HashMaps to set up all of the cards - I thought of using an <Integer, String> pair to first number all the cards and then pair them with their values. For example, maybe the 3rd card in the (unshuffled) deck would be a Goal card with a goal of obtaining the Moon and Sun Keeper cards,so it would look something like: <3, "Goal:Moon, Sun">. This format would be similar for the Action, Keeper, and Rules cards as well. Since each card would be numbered, it would allow for organization when shuffling the cards and distributing them to players as well as discarding them. The main class would have a start method to loop through each player and have them draw cards based on the current rule - based on the card each player would draw, the correct class would then be called. This would continue until someone matched their Keeper cards to the current Goal. For the methods that return multiple values, they will first be organized into a list which will then be parsed to organize. Game ends when Game_Is_Won is set to True.


### CRC Card Classes

1. This class's purpose or value is to initialize the Fluxx Game at the start of play and to continue to update as new players are added. This class also contains the main game play:

|Game_Setup| |
|---|---|
|Public Variable (Pair) Basic_Rule         |Action|
|**Initialize_Players Method:** *parameters* =  (int) number_of_players;  *return* =  number_of_players | Goals |
|**Initialize_Cards Method** *parameters* = (int) number_of_players, (HashMap) card_deck; *return* = (HashMap) new_card_pile, (HashMap) discard_pile, (HashMap) cards_in_hand   | Keeper |
|**Add_Players Method:** *parameters* = (int) number_of_players, (int) players_to_add; *return* = (int) number_of_players (number_of_players + players_to_add)| Rules |
|**Void Start_Play Method** *parameters* = (int) number_of_players, (HashMap) cards_in_hand, (HashMap) new_card_pile, (HashMap) discard_pile ; *classes* = (Action) (Goal) (Keeper) (Rules) ||
|Public Variable (Pair) Current_Rule ||
|Public Variable (Pair) Current_Goal ||
|Private Action Action_Card - Will call Read_Action method ||
|Private Goals Goal_Card - Will call Read_Goal method||
|Private Keeper Keeper_Card - Will call Read_Keeper method ||
|Private Rules Rules_Card - Will call Read_Rules method||
|Public Boolean Game_Is_Won ||
```java
public class Game_Setup{
    //sets up Fluxx game with initial players, serves as main class as game play continues
    public int Initialize_Players (int number_of_players){
        // initializes the number of players in game
    }
    public ArrayList<HashMap<Integer, String>> Initialize_Cards (int number_of_players, HashMap<Integer, String> card_deck){
        // shuffles cards, deals out 3 cards to each player (cards_in_hand for each), all of the cards that have not yet been dealt will be in the new_card_pile, and an empty discard_pile will also be created
    }
    public int Add_Players (int number_of_players, int players_to_add){
        // returns new number of players
    }
    public void Start_Play (int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> new_card_pile, HashMap<Integer, String> discard_pile){
        //for loop to loop through each player for their turn, player will follow the global "base rule" card
        // communicates with other classes to keep track of game play
    }
}
```
2. This class's purpose or value is to deal with the Action cards within the game, and communicate with the necessary card classes as well as the main game source.
   
   |Action| |
   |---|---|
   |**Change_Keepers Method:** *parameters* = (int) number_of_players, (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile; *return* = (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile        |Game_Setup|
   |**Change_Goals Method:** *parameters* = (pair) Current_Goal, (int) number_of_players, (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile; *return* = (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile   | Goals |
   |**Change_Rules Method** *parameters* = (pair) Basic_Rule, (int) number_of_players, (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile; *return* = (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile   | Keeper |
   |**Change_Cards Method:** *parameters* = (int) number_of_players, (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile; *return* = (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile| Rules |
   |**Repeat_Turn Method:** *parameters* = (int) number_of_players, (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile; *return* = (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile ||
   |**Rotate_Players Method:** *parameters* = (int) number_of_players, (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile; *return* = (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile||
   |**Public Void Read_Action** *parameters* = (HashMap) Action_Card || 
   |Private Goal Goal_Card ||
   |Private Keeper Keeper_Card ||
   |Private Rules Rules_Card ||
   |Public Variable (Pair) Basic_Rule ||
   |Public Variable (Pair) Current_Rule ||
   |Public Variable (Pair) Current_Goal ||
```java
public class Action{
    //When an Action card is drawn/used, this class deals with the possible outcomes
   public void Read_Action(HashMap<Integer, String> Action_Card){
       //This is the method that the another class will call, this method will then call one of the necessary methods below depending on what the Action is.
   }
   public ArrayList<HashMap<Integer,String>> Change_Keepers (int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> cards_in_hand, HashMap<Integer,String> discard_pile){
        // If the Action card has anything to do with the Keeper card (Exchange Keepers, Steal a Keeper, etc.) then this method will call the Keeper class with the Keeper_Card object.
    }
   public ArrayList<HashMap<Integer,String>> Change_Goals (Pair<Integer, String> Current_Goal, int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> cards_in_hand, HashMap<Integer,String> discard_pile){
        // If the Action card has anything to do with the Goal card, this method will call the Goals class using the Goal_Card object
    }
   public Arraylist<HashMap<Integer, String>> Change_Rules (Pair<Integer, String> Basic_Rule, int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> cards_in_hand, HashMap<Integer,String> discard_pile){
        // If the Action card has anything to do with the Rules card (Trash a New Rule, Rules Reset, etc.) then this method calls the Rules class using the Rules_Card object
    }
   public ArrayList<HashMap<Integer, String>> Change_Cards (int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> cards_in_hand, HashMap<Integer,String> discard_pile){
        //If the Action card has something to do with card, but not a specific type (Draw 2 and Use 'Em, Zap a Card!, etc.) then this method will just deal with that itself
    }
   public ArrayList<HashMap<Integer, String>> Repeat_Turn (int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> cards_in_hand, HashMap<Integer,String> discard_pile){
        //If the Action card has the player repeat a turn (Take Another Turn), then this method will do so
   }
   public ArrayList<HashMap<Integer, String>> Rotate_Players (int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> cards_in_hand, HashMap<Integer,String> discard_pile){
        //If the Action card has players rotate cards (Rotate Hands) then this method will do so.
   }
}
```
3. This class's purpose or value is to deal with the Rule cards within the game, changing the current rule (in comparison to the basic rule) and communicating with the necessary classes.
   
   |Rules| |
   |---|---|
   |**Change_Keepers Method:** *parameters* = (pair) Basic_Rule, (int) number_of_players, (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile; *return* =  (pair) Current_Rule        |Game_Setup|
   |**Change_Goals Method:** *parameters* = (pair) Basic_Rule, (pair) Current_Goal, (int) number_of_players, (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile; *return* = (pair) Current_Rule   | Goals |
   |**Change_Action Method** *parameters* = (pair) Basic_Rule, (int) number_of_players, (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile; *return* = (pair) Current_Rule | Keeper |
   |**Change_Cards Method:** *parameters* = (pair) Basic_Rule, (int) number_of_players, (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile; *return* = (pair) Current_Rule| Action |
   |**Public Void Read_Rule** *parameters* =  (HashMap) Rule_Card ||
   |Private Goal Goal_Card ||
   |Private Keeper Keeper_Card ||
   |Private Rules Action_Card ||
   |Public Variable (Pair) Basic_Rule ||
   |Public Variable (Pair) Current_Rule ||
   |Public Variable (Pair) Current_Goal ||
```java
public class Rules{
    //When a Rule card is drawn/used, this class deals with the possible outcomes
   public void Read_Rule(HashMap<Integer, String> Rule_Card){
       //This is the method that another class will call, this method will then call one of the necessary methods below depending on what the Rule is.
   }
   public Pair<Integer, String> Change_Keepers (Pair<Integer, String> Basic_Rule, int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> cards_in_hand, HashMap<Integer,String> discard_pile){
        // If the Rule card has anything to do with the Keeper card (Keeper Limit 2, Keeper Limit 3, etc.) then this method will call the Keeper class with the Keeper_Card object.
    }
   public Pair<Integer, String> Change_Goals (Pair<Integer, String> Basic_Rule, Pair<Integer, String> Current_Goal, int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> cards_in_hand, HashMap<Integer,String> discard_pile){
        // If the Rule card has anything to do with the Goal card (Goal Mill), this method will call the Goals class using the Goal_Card object
    }
   public Pair<Integer, String> Change_Action (Pair<Integer, String> Basic_Rule, int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> cards_in_hand, HashMap<Integer,String> discard_pile){
        // If the Rule card has anything to do with the Action card (Goal Mill etc.) then this method calls the Action class using the Action_Card object
    }
   public Pair<Integer, String> Change_Cards (Pair<Integer, String> Basic_Rule, int number_of_players, HashMap<Integer, String> cards_in_hand, HashMap<Integer, String> cards_in_hand, HashMap<Integer,String> discard_pile){
        //If the Rule card has something to do with card, but not a specific type (Play 2, Mystery Play, etc.) then this method will just deal with that itself
    }
}
```

4. This class's purpose or value is to deal with the Goal cards within the game, changing the current Goal anytime a new Goal is played.
   
   |Goals| |
   |---|---|
   |**Public Void Read_Goal** *parameters* = (HashMap) Rule_Card |Game_Setup|
   |**Change_Goal Method:** *parameters* = (Pair) Current_Goal; *return* = Pair (Current_Goal) |Action|
   |Public Variable (Pair) Current_Goal |Rules|
   | |Keeper|
```java
public class Goal{
    //When a Goal card is drawn/used, this class deals with the possible outcomes
   public void Read_Goal(HashMap<Integer, String> Goal_Card){
       //This is the method that another class will call, this method will then call the method below to change the Goal.
   }
   public Pair<Integer, String> Change_Goal (Pair<Integer, String> Current_Goal) {
      // This method will replace the current goal
   }
}
```
5. This class's purpose or value is to deal with the Keeper cards within the game, changing them for each player and checking them as needed
   
   |Keeper| |
   |---|---|
   |**Public Void Read_Keeper** *parameters* = (HashMap) Keeper_Card |Game_Setup|
   |**Change_Keeper Method:** *parameters* = (HashMap) cards_in_hand; *return* = (HashMap) cards_in_hand, (HashMap) discard_pile, (HashMap) new_card_pile | Action |
   |**Check_Keeper Method:** *parameters* = (HashMap) cards_in_hand, (Pair) Current_Goal); *return* = (Boolean) Game_Is_Won |Rules|
   |Public Variable (Pair) Current_Goal |Goals|
   |Public Boolean Game_Is_Won ||
```java
public class Keeper{
    //When a Keeper card is drawn/used, this class deals with the possible outcomes
   public void Read_Keeper(HashMap<Integer, String> Keeper_Card){
       //This is the method that another class will call, this method will then call the method below to change the Keeper.
   }
   public ArrayList<HashMap<Integer, String>> Change_Keeper (HashMap<Integer, String> cards_in_hand) {
      // This method will replace the current keeper
   }
   public Boolean Check_Keeper(HashMap<Integer, String> cards_in_hand, Pair<Integer, String>Current_Goal){
       //This method checks to see if Keeper cards match the goal, if yes then it returns true and the game ends
   }
}
```
