# Journal : Ethics
### Reya Magan


## Ethics Essay

#### Personally helped by computing technology
I would say that the biggest way that computing technology has helped me is through allowing me to stay connected to friends and family abroad. I think that the creation of apps like FaceTime and other social networks have been very important in preserving relationships from thousands and thousands of miles away. As technology continues to grow, these applications become more versatile and connecting with your loved ones becomes much more simpler. An example would be ZOOM, as it allowed people to stay connected during the pandemic, and education was able to continue as well.


#### Personally harmed by computing technology
I think there are always security issues surrounding the use of computing technology. As much of our daily life continues to move online, there are multiple ways that we can be harmed. For example, with online banking - sometimes bank accounts can get hacked and cause extreme loss. There have been instances in my family as well as with my friends that phishing and scam links have caused bank information to be compromised. This can be really harmful, as a lot of money gets lost and cannot be recovered. Though there are multiple security measures implemented by banks to prevent this from happening, it is still a problem present in society.

#### Computing technology trend for society
I think computing technology is generally positive for society. Though there are risks involved with security and spread of misinformation, overall computing technology's benefits outweigh its risks. This technology gives us access to a limitless amount of information, and allows us to connect to loved ones and like minded individuals all around the world. I think technological development and societal development go hand in hand: as technology continues to improve and become more versatile and strong, we as a society can achieve more too. 


#### Computing technology's role in ethics
I think computing technology definitely poses ethical concerns and risks. Beyond security concerns, computing technology allows for any information to be posted anywhere. Anyone can post their opinions without any filter online - and this can lead to spread of racism, sexism, and hate in general. This is hard to regulate as well, as sometimes in brings into question how much censorship can be allowed in cases of online posting. Still, there should be some regulation - for example websites like Twitter have implemented measures to censor harmful and hateful content.

#### Article Response
I agree a lot with the article that ethics needs to be introduced in computer science academia. Since computing technology has a strong impact on human interactions, it is important that not only does it work, but that it is also ethically sound and just. In fact, I was involved with a similar issue surrounding ethics in engineering. Since engineering and computing technology rely a lot on human centered design, it is important to consider all the tradeoffs and create solutions that do not cause too much harm. It is important to stress in academic environments that while a working solution is important, it is valid to make certain decisions that may compromise some functionality but are more ethically sound.



## Failure Essay

### Personal Experience
I think that failure is extremely important in the journey to success. Going through middle and high school, I did not face very much failure: I was a good student, and I got into a good college. Coming to Duke, however, I was immediately met with a shock when I wasn't able to keep up with my classes, and was receiving poor grades at the start of my college career. In the beginning, I was trying to get a biomedical engineering degree on the pre-med track, but I was having a very difficult time with all of my bio/chemistry classes. It became repetitive: I would take a bio/chem class, perform poorly, and take another one. I had people around me suggest a degree in computer science, but I was more comfortable in my failure than the unknowns of a new major. Ultimately, I made the switch, and though there was an adjustment period, I ultimately realized I made the best decision for myself. So failing is really important, but it is very important to learn from this failure and often go out of your comfort zone in doing so.


### Positive reframing

* Failure is important

* Failure is a momentary event

* Fail often in order to succeed sooner

* Failing is easy; trying again takes courage

* Failure can be controlled
