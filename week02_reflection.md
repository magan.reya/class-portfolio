# Journal : Reflection
### Reya Magan


## Reflection

### Reflection #1
This week, I found the Hangman project to be 
really challenging. I especially found the clever players to be 
really hard to implement as it took me a while to understand exactly what their 
function was. I utilized the CS 101 HOWTO page quite extensively to 
understand this concept, and practiced possible
debugging scenarios by hand before understanding how to implement it
in my code. I think it was really helpful to see the HOWTO page, and 
definitely writing it out on my own before jumping into coding helped
me to summarize my ideas and translate them into code
easier.
### Reflection #2
I did this project without consulting anyone, but I did improve my soft skills still.
When it got really late at night but there was still coding to be done, I started to talk myself
through the process as if I was teaching myself. This helped me to communicate my ideas in a concise manner
(albeit to myself) and then write code in a more organized way. I know this is still a solo
process, but this type of thinking out loud really helps me to communicate my knowledge well, and I think it
will be useful once team projects start.

### Reflection #3
I learned a lot about refactoring some of my constants into public static final variables this week. At first,
I had a lot of "magic numbers" lying around, and I was using private/public declarations quite haphazardly, but after
learning to refactor I was able to change a good amount of that. I made all of my constant JavaFX related variables into 
public static final objects, which meant that they could be accessed in any class, only one instance of them would exist,
and they would not change throughout the project. This made it helpful in writing my Hangman code.

### Reflection #4
I was thinking back to my internship while doing this project, and I think that already in two weeks of this course I have written cleaner code than I did during my internship. Once I start
thinking about my code, my mind goes into auto pilot and I just start writing and writing - and it often gets pretty messy. That still happens, as I am still training my mind to think in a cleaner way, but now each time I write code I actively try to think about how it could
be made more readable or how else I could write it. I still have a long way to go, but this is definitely really helpful for the real world.



## Summaries

### Summary #1
The biggest thought-work I did this week was 'testing' the clever keeper and guesser by hand. It took me a while to understand the concept,
so I stepped through all of the CS 101 examples until I could summarize to myself how the process worked. This was really helpful because once I understood
the logic behind the clever players, implementing them did not take very long. I think this type of thought work is really good because it leads (for me at least) to more organized
thinking when it comes to writing code.

### Summary #2
One design decision that I made this week was to only allow a fixed number of guesses for the Hangman game. I set this as 8, so the user only has 8 guesses until the whole person is drawn and the game ends. The pros to this were that I didn't have very many issues 
with bugs when it came to setting up a display for the hangman visual. The person continued to get more features as wrong guesses were made, and the game was able to continue until all 8 guesses were missed. The con was that this makes the game much more
basic and elementary - I think it would have been really cool to add more features and options to the game (like an easy/medium/hard level), but ultimately to save time I chose to keep it fixed. I think I would definitely add more features if given more time.



## Feedback
At my meeting, my TA suggested having more classes for my Breakout game. I originally only had the Main class, an InitializeElements class, and a Collisions class. These classes worked well, but my TA suggested having a class for every element of the game (Ball, Brick, Paddle, etc.). I went back and implemented this, as well as a few smaller issues (camel case naming, getting rid of magic numbers). I think I can still work on making some methods inside my classes smaller, though. I noticed in my Hangman game that there are a few methods that run large, so definitely I would want to try to play around to make those shorter and easier to read.
