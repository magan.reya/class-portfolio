CompSci 307 : OOLALA Team Review
===
## Team Number: 9
## Name: Reya Magan
## Date: 10/12/2021


### Contributions and Roles

* Describe your role(s) and your satisfaction

I was primarily involved with designing the frontend for this project. I implemented a general GameView class that was abstract
and had the Logo, LSystem, and Darwin classes extending this. I implemented the majority of the features and also set up multi-language
resource files and css set up. I also did a lot of the error handling and display, and added some functionality for commands. I created factories and event
call packages to keep track of JavaFX elements in a modular and clean way. I also helped out with debugging the backend whenever issues arose. Finally, I added a lot of 
functionality to knowing the positions and general information about the ImageView animal objects. I feel pretty satisfied with my role,
I feel that I did not have much skills in frontend design, so this project definitely helped me improve on that. I also had struggled a lot with inheritance throughout the semester so far, but
I was able to implement it well for the view and I think I have a strong understanding now.

* Describe your team mate(s) role(s) and their satisfaction

Kristen was primarily involved with the backend. She created the controller and model connections to keep track of instructions
throughout the game and also parse LSystem and Darwin instructions from basic Logo instructions. She used a lot of abstract classes to set this up,
along with Animal/AnimalData classes to keep track of the Animals on the screen and any of their relevant data. She also helped debug the view when there 
were issues. I think Kristen is satisfied with her role, she utilized inheritance really well and also improved her understanding of different programming skills,
like regular expressions. 

Will was involved with supporting the view and the backend. He implemented the tell command, and set up Animal ids within AnimalDisplay. He also added some view features like set pen color and set background color. I think Will is satisfied with the work he did, but he mentioned that he would have wished to be at more initial meetings since he didn't have a good understanding of the design choices.

### Team Events

* Describe when you felt the most satisfaction

I felt most satisfaction at myself when I was able to successfully implement GameView as an abstract class with Logo, LSystem, and Darwin GameViews extending it.
I have been having a lot of difficulty understanding inheritance during this semester, but I took on the challenge in order to make the view better designed. I think it really paid off,
as now it is easy to trigger the selected game, and easy to follow each of the different game views. I felt really proud that I was able to solve something that I had really been struggling with.


* Describe when you felt the team made the biggest leap ahead

I think the team made the biggest leap ahead when we figured out a way to translate Logo commands into LSystem commands, and easily display differences between the games on the view. I pair programmed a lot with Kristen during this, and was very impressed by how quickly she figured out ways to parse LSystem commands using regular expressions,
I think this is something I would like to try more next project as well. I worked on the view concurrently as she worked on the backend, and when we merged our LSystem set up together it worked really well, and it was extremely satisfying knowing that we were able to extend our Logo instructions to a new game fairly easily.
* Describe way(s) the team has worked well

I think everyone on the team was really respectful and wanted to help out in every way they could. We were all considerate of each others' ideas, and provided
constructive criticism whenever it was needed.

* Describe problem(s) with the way the team worked together
Since Will was not present at some initial meetings, there was some disconnect in his understanding of the design ideas/choices. I think it was difficult to keep him on the same page while also moving forward, so often 
times we ended up having a lot of merge conflicts because new features would be added to older versions of the code that had since been changed.

### Learning Opportunities

* Your biggest strength as a team member

I think I am willing to compromise and am open to all suggestions. I also make sure to get everything I need to get done done well, and in a timely manner. I was also willing to help whenever it was needed.

* Your biggest weakness as a team member

I really feel like I am still improving in my programming abilities in general, so sometimes I felt maybe I wasn't at the same knowledge level as my partners. I think I need to keep working harder to improve my
understanding of programming functionality so that I can be even more helpful in future projects.

* What you have learned team collaboration/communication during this project

I learned that it can be difficult to keep everyone in a team on the same page while also moving forward. While on one hand I think it is important to be proactive and ask questions whenever there is a disconnect,
I feel that I should have also been more proactive in making sure that everyone was on the same page with the design. As the project was time sensitive, it was important to move forward, but I think for next time I would
want to make sure everyone is on the same page so everyone feels they know what they should work on.

* A specific thing can you do to make team collaboration/communication more productive

As mentioned above, I think I need to be more proactive in making sure people are on the same page with the design, and that people consistently merge with each other to lower risks of merge conflicts in the future.
Moving forward is important, but having everyone on the same page is as well.