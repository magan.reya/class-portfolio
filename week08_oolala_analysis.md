# Journal : OOLALA Project Analysis
## Name: Reya Magan
## Team: 9
## Date: 10/17


Design Review
=======

### Adding new Turtle Graphics

If someone wanted to add a new turtle graphic application to this project, it would be fairly easy to do so. 
These are the steps that they would need to follow:
1. Data Files: For the new game, we would need new data files that can be loaded and ran within the application.
   The data folder already has darwin, logo, and lsystem packages, so we could just create a new package for the new
   game, and add the necessary files of instruction sets.
2. Commands: If this new game has any unique commands that do not translate into already existing commands (the way some LSystem and Darwin games are based on Logo),
   then we would need to add new command classes that extend the already existing abstract command class. This would be easy to do so as we add the new command class, and have it
   override the already existing executeInstruction abstract method from the abstract command class. This method would be implemented given the specifics of this new command.
3. Controller: The controller has a few classes that may need to be updated. They are as follows:
   1. CommandTranslator: We already have a command translator that takes certain instructions (say "MOVE" in Darwin or "F" in LSystem) that can be simplified to 1+ basic Logo commands using a hashmap to map the current game instructions (key) to equivalent logo translations (value)
      If our new application relies on basic Logo commands in any way, we would create its own translator class that extends the abstract CommandTranslator class. We would map the instructions for this new game to any Logo instructions that they are translated from, and this would allow us to not
      need to create new command classes extending the abstract commands class.
   2. Controller: If in fact we need a translator for this game (which we likely will), we need to update the setMyClassesForGameType method within the GameController class (line 113). This is the method that sets up the right translator and animal classes for the game, so we would need to add the calls for the new translators and managers within this method.
4. Model: The model has a few classes that may need to be updated. They are as follows:
   1. Animal: The animal class deals with the animal that is actually doing the actions. Each animal is initialized with an ID so it knows which commands it needs to execute. The abstract animal class has methods that every animal type needs to consider (moving, rotating, detecting walls, etc), and we have
   DarwinAnimal and DrawingAnimal classes that extend this abstract for more specific behavior (infecting for Darwin, drawing for Logo and LSystem). If the new game can fit well into either a DrawingAnimal or a DarwinAnimal, we would not have to add much at all.
   Likely, though, it would have some of its own unique features that need to be implemented. In this case we could make its own class extending the abstract animal class. If this new animal type has any features that match one of the original but not the other (has a similarity with Darwin but not with Drawing), we may need to put this method into the abstract class and override it within each subclass (and to have it just return nothing for the class that doesn't need it).
   2. AnimalManager: This abstract class basically organizes all the possible Animals within a game and keeps track of the commands for each one. Just like the abstract Animal class, there is a DrawingManager and a DarwinManager that extend these classes. For the new turtle application, if this application has unique Animals and command distributions, we would just create its own new class with valid override abstract methods and any unique methods for the new game.
5. View: The view has a few classes that may need to be updated. They are as follows:
   1. AnimalDisplay: Currently the AnimalDisplay is a standalone class that deals with displaying an ImageView animal with its correct positional and drawing data. In the current implementation, we would add any unique to the new game methods right into this AnimalDisplay class. That is not the best way to do this, so it would have been good to have an abstract AnimalDisplay class with extended methods like the Animal and AnimalManager classes. We would then just add a new extended class with any unique functionality.
   2. GameView: We have an abstract GameView class that sets up common view elements between the three original games. For any unique functionality, we extend this class within a specific game view (eg LogoGameView). For the new turtle application we would just need a new GameView class for it that extends the abstract GameView class. We could then override any methods such as makeVerticalPanel() (line 197) to add whatever features we want to the vertical box on the game display. We would be able to easily take what we need from the GameView class and add any
   unique functionality for the new game as well.
   3. GameSelect: The game select view offers the users the ability to choose which game to play. All we would need to do here is add the new game to the resource bundle "ChoiceDialogText". Then since in the ChoiceDialogFactory class we add everything from the resource bundle key set to our choice dialog, this new game would be automatically added. Within the NewGameEvents class (called at the beginning of the game) we pass this game to the controller, so the controller would have this new game and run all the required extended subclasses for it.  

All of the features for this new game could be added without really having to touch much existing code at all, we would really just need to make more extended classes within the abstractions we already have.



### Overall design
For this project, we used an MVC set up to connect all of our main components (model, view, controller) together for ease of readability and functionality within the game. We designed the code so that first the user is prompted to select a certain game to play (GameSelect class, makeGameDisplay() method). This method will return a new GameView class that has the selected game as well as the language that the user chose to play in.
Then we initialize the model as just a new GameModel class, and pass both this newly created GameView and GameModel into our controller. We also set the controller within the view and start the animation. Then we start at the makeScene method within the view and the game begins to run as a result of user inputs. So, since we know which game was selected, we know that the specific game view class is being used. Lets assume the user chose to play the Logo game. 

If the Logo game is selected, this information is first passed to the controller using the myNewGameEvents.selectGame() call within the GameView makeScene method line 120. Within the controller then, this would set the CommandTranslator as a LogoTranslator, and the AnimalManager as a DrawingManager (because Logo deals with drawings). Now that the controller also knows which game is being played, the rest of the game is dependent on user actions. Within the game scene, there are multiple buttons a user can press to play the game. Each of these buttons leads to changes within the controller and model, and then is also reflected within the display. We can step through one example:

1. The user presses the Load Command File button: This button was created within the GameView class method makeUserInputs() (line 156). This method uses a ButtonFactory to create this button, and then links it to the CommandEventCalls class to deal with the action after this button is clicked. So now we are in this event call class.
2. Within the event call class: Since the user loaded a file, we will go into the loadCommandFile method in the CommandEventCalls class (line 35). This method pops up a file chooser, and the user can select the file they would like to load. It is here where the connection to the controller is made: the file that is chosen is sent to the createListofIsnsFromFile method within the controller for translations of commands. 
3. Within the createListOfIsnsFromFile method: This is line 176 of the GameController class. This method will take in a file string, and read the file within the CommandTranslator to translate to Logo if needed. Since we are running the logo game no translation is needed, simply the file is read and each line is added to a list. Then we go back into the loadCommandFile method within the CommandEventCalls class, add the commands to history, and then call the createCommands() method within the controller to process the commands.
4. Within the createCommands method: This is line 211 of the controller. This method calls the CommandGenerator to parse through the list of command strings and match them to the actual command class.
5. Within the CommandGenerator: This class will use methods such as createCommands() (line 41) and generateCommandObject (line 71) to parse through the commands list and use reflection to map the command strings to a class ("fd" to MoveForwardCommand). This class will return a queue of all of the Command classes in order from the file. We then go back into the controller and add this queue to the addCommands method within the Model.
6. Within addCommands method in Model: This method will call the addCommands method within the AnimalManager class.
7. Within addCommands method in AnimalManager: We would be in the DrawingManager for Logo, so this method in DrawingManager will add the commands queue to the specified animals. For Logo we typically deal with one animal (except in the case of tell command), so usually this would take the Queue and add it to the animal using the addCommands method within the animal class.
8. Within addCommands method in Animal: This method will just add all of the commands to a linked list that holds the commands that a Animal has to execute.

So the top 8 steps deal with passing the commands from frontend to backend. While this is happening, we also have an animation running that calls the step() function in the controller every 0.25 seconds. It is as follows:
1. Within the view: In the start() method (line 104) we have a timeline that calls the step() funciton in the controller every 0.25 seconds.
2. Step() function in controller: This method calls the executeStep() method within the Model.
3. executeStep() in the model: As long as there are instructions to be executed, this method will call executeCommand() within the AnimalManager class.
4. executeCommand() in Animal Manager: This class is abstract so we look at its implementation in DrawingManager. Basically, we call the executeCommandsOnSpecifiedAnimals() to execute the given commands on an animal. 
5. executeCommandsOnSpecifiedAnimals() in Animal Manager: This class looks at the getNextCommand() from the Animal class and basically executes each command until there are none left. This then goes back into the model with all of the updated positional and drawing data for the Animal.
6. Back in the executeStep() in the model: We now update the data map in the updateDataMap() method to update the AnimalData for the Animal following the execution of all its commands. We then go back to the controller.
7. Back in step() in the controller: We call the update function in the view to show all of the updated Animal states after the commands by passing the new animal data into the view. This then shows all of the changes. This happens every 0.25 seconds.

So as we can see, just one button click accomplishes many tasks within our design. We utilize our controller as a middle point between the model and the view and thus can easily process commands to be run on animals, update their data, and then show visual proof of these changes within our view in a matter of 0.25 seconds.



### Design Principles

#### Clean Code

* Good or __Needs Improvement__
```java
protected void updateFromData(AnimalData newData, Group root) {
    if (!idMap.containsKey(newData.getMyID()) && fileMap.containsKey(newData.getMyType())) {
      addNewAnimalFromData(newData, root, INITIAL_POSITION, INITIAL_POSITION);
      return;
    }
    ImageView animalToUpdate = idMap.get(newData.getMyID());

    double pastX = animalToUpdate.getBoundsInLocal().getCenterX();
    double pastY = animalToUpdate.getBoundsInLocal().getCenterY();

    updateAnimalLocationAndVisibility(newData, animalToUpdate);
    resetImage(newData.getMyType(), animalToUpdate);
    checkAndChangeImage(animalToUpdate);

    if (newData.getStamp()) {
      stampTurtle(newData, root, animalToUpdate, newImagePath);
    }
    if (newData.getPenStatus()) {
      drawLine(root, animalToUpdate, pastX, pastY);
    }
    if(addImage){
      stampTurtle(newData, root, animalToUpdate, addImagePath);
    }
  }
```
For clean code, there is a class that I think needs some improvement.
The AnimalDisplay class within the project is quite long, and it is hard to follow as it contains methods that are needed only for specific games such as Logo only, or LSystem only. I feel that we would need to improve this design so that we could follow the 
Clean Code Principle. My idea for this is to make AnimalDisplay an abstract class, and then have an extended AnimalDisplay class for each game type. There are some methods such as addNewAnimalFromData or updateAnimalLocationAndVisibility that are required in all three games,
but the way there are called in the AnimalDisplay class currently is difficult to understand. There is an updateFromData method (see above) that not only updates the images to be used on the scene, but also the position of the images, and the line that is drawn. This method is used in Darwin and Logo/LSystem,
but it seems unnecessary to call methods such as getStamp() and getPenStatus() for something like Darwin when in fact we know that Darwin does not have any line drawing or stamping involved. So I think this method would need to keep common functionality in our abstract AnimalDisplay class, but then be overridden for any extra features independent to one game.
I think we still need to make the calls to updateAnimalLocationAndVisibility and resetImage methods within this method, but in the extended and overridden methods within DrawingAnimalDisplay (Logo and LSystem), we would add the stamping and drawing methods (lines 80-88 in the original method). This way, we would not be calling methods where they are unneeded.
The drawline, setStrokeColor, checkAndChangeImage, changeImage, setLineThickness, and stampTurtle would only apply to DrawingAnimalDisplays, so they would be moved to that extended class, making our abstract AnimalDisplay class much shorter and much more readable and generalizable. The DarwinAnimalDisplay class then would also only have methods that are needed,
including the general AnimalDisplay methods needed for each game (like resetImage) and also a file map with a list of many animals that can be placed on the Darwin scene. I think having this abstraction with extended classes would make the AnimalDisplay set up much easier to follow and generalize for new games that could be added too.


#### Single Responsibility

* __Good__ or Needs Improvement
```java
package oolala.view.factories;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

import java.util.ResourceBundle;

public class ButtonFactory {


    /**
     * This method/class makes all the Button nodes seen on the scene, and maps them to an ActionEvent. For example,
     * the New Game button triggers a NewGameEventCall when pressed.
     * @param myTextDisplays: This resource bundle has all of the titles for the buttons that are seen on the screen
     * @param label: This is the key that is used to find the relevant Button title within the myTextDisplays resource
     *             bundle.
     * @param handler: This is the ActionEvent that is triggered when a button is clicked
     * @return result: The button that has been created as a result of this factory.
     */
    public Button makeButton(ResourceBundle myTextDisplays, String label, EventHandler<ActionEvent> handler){
        Button result = new Button();
        result.setText(myTextDisplays.getString(label));
        result.setOnAction(handler);
        return result;
    }
}
```

One way I think Single Responsibility was done well was within the factories in the view package. The main role of the GameView class itself was to set up the scene and update as needed. In setting up the scene there are multiple different JavaFX elements that need to be included. At first, each of these elements were being created within the GameView class itself, but this became very long and there were multiple things happening within GameView such that it no longer followed the single-responsibility principle (create elements, setting up the scene, updating the scene, calling action events). To combat this, the factory package within the view package was created. Now, the GameView class simply sets up the scene using methods such as makeUserInputs() which call factories to create elements and event call classes to create action events. These factories are a great example of single-responsibility principle as they simply create the JavaFX element that is requested. For example, above we can see the ButtonFactory class. This class is called every time a button is created.
All we need is the resource bundle that has all the button titles, the key string that points to the label we want from the bundle, and the event action that is to be called when the button is clicked. All of the other factories are similar to this, they set up the element using resource bundles and initialize any other information we need when creating a JavaFX element. I believe this follows single-responsibility principle well as the factory classes only have one role: create JavaFX elements, and that is all they do. It also gives the view only the responsibility to display these elements and changes to the scene, so now the view also only has one responsibility.




#### Open Closed

* __Good__ or Needs Improvement
```java
package oolala.commands;

import oolala.model.Animal;
import oolala.model.AnimalManager;

/**
 * The command class is a representation of a command that will be applied to an Animal, and will
 * cause it to take an action. This abstract class is the general format for commands, and
 * can be extended to create commands with specific functionality. This class may fail if the
 * incorrect type of Animal is passed to it, or if there is not an AnimalManager passed to it. It
 * is dependent on having an Animal and AnimalManager passed to it that it can act upon. An
 * example of using this class would be to extend it to create a command with specific
 * functionality, like the MoveForward command, and someone could implement the executeInstruction
 * method for that class to do the specified functionality.
 */

public abstract class Command {



  private double myValue;

  /**
   * Executes command on current animal, makes a change to that particular animal/animal manager. We
   * assume that there's an Animal and an Animal Manager that have been passed, and that the
   * specified functionality for the command has been implemented correctly
   *
   * @param currentAnimal current animal passed to act up on
   * @param animalManager the game's animal manager that knows about and can edit all animals in the
   *                      game
   * @return boolean whether the instruction has been completed
   */
  public abstract boolean executeInstruction(Animal currentAnimal, AnimalManager animalManager);

```
One way I think the Open-Closed Principle was done well was through the abstract Commands class. This class had the abstract executeInstruction() method that took in the current animal to act on and its manager that could deal with what to do with the animal itself. We can see the abstract method above, but this class was very well abstracted such that for each new game we simply had to 
add a new class for that command that implemented its own unique, overridden method of executeInstruction(). We were able to have every Logo Command extended from this class, as well as all of the Darwin commands that did not derive from Logo (IfEnemy, IfWall, etc.). The only change we had to make each time was that we added
a new class, and implemented its own version of executeInstruction. The abstract command class did not need to be modified to hold commands of different types of games. I believe thus, that this is a great usage of the Open-Close principle as the commands abstract class was open to extension (through each of the extended commands classes), and closed to modification (the abstract class itself did not need to be changed).


### Code Design Examples

#### Made easier to implement by the code's design

* Feature: LSystem Implementation, Darwin Implementation

* Justification: For the basic deadline for this project, we had implemented complete functionality for the Logo Application. Going into the next step, we had to figure out a way to extend what we had already created for Logo into Lsystem and
  Darwin. Since we had a good structure with our abstract Commands class in Logo, we thought we could take certain commands within LSystem and Darwin (like "F" or "MOVE" respectively), and translate them back into basic Logo commands that already existed as classes. We discussed ideas for this for a while, and eventually settled on a sort of "translator" class that would take a command from LSystem or Darwin
  and translate it into a basic Logo Command. We first tried this out with LSystem, and found that since we already had strong design for Logo Commands it was really extendable to just use regular expression parsing mechanisms to read LSystem commands and call the corresponding Logo Command based on a map that was set up to hold these translations. Compared to the time it took for full Logo functionality, it was fairly quicker to get LSystem up and running because we really just needed to translate the majority of commands back into a Logo game. Darwin was similar for the commands that depended on Logo, at this point since we already had a well established translation mechanism we were able to do a similar process as LSystem by mapping commands like MOVE to fd and then setting up regular expression parsing mechanisms to translate Darwin commands into Logo commands. So I believe that the abstract Commands class and CommandTranslator class were really useful in extending our design to include multiple applications, and I think that it would be very well extendable for any new application that could be added as well.



#### Good Example **teammate** implemented

```java
private Command generateCommandObject(String currentIsn) {
    try {
      return (Command) Class.forName(
              String.format("oolala.commands.%s", myInstructionSet.getString(currentIsn)))
          .getConstructor().newInstance();
    } catch (Exception e) {
      myErrorHandler.setErrorFound(
          String.format("%s %s", myErrorsBundle.getString("IsnHelpMessage"), currentIsn));
    }
    return null;
  }
```
* Design: CommandGenerator class, especially method generateCommandObject() (above). My teammate utilized reflection to call appropriate command classes dependent on a passed command string. They created a resource bundle that mapped commands to their relevant classes, and thus with one line of code we could map a given key to a class.


* Evaluation: I feel like this entire class, but especially the generateCommandObject() method is a strong example of the clean code principle. The generateCommandObject() method uses reflection to call the appropriate Command class based on the command string that is passed in. Before implementing reflection for this, the class had if statements checking a key within the LogoInstructions resource bundle, and calling a command class based on that ("lt" was equal to "TurnLeftCommand"). This worked well, but since it was quite long with multiple if statements, it was not the cleanest way to do this. Our team struggled a lot with how to implement this, since we could not think of any way besides if statements or switch cases. After discussing with some TAs they mentioned that for the first project it was OK if this method did not follow the clean code principle because we hadn't learned about the reflection tool yet. I was really impressed, however, when my teammate took the initiative to learn reflection from their TA and implement one line of code instead of the 10+ if statements that were originally there. I feel like this follows the clean code principle extremely well and thus I would like to highlight it as a great addition by my teammate. 

#### Needs Improvement Example **teammate** implemented
```java
private void setBackgroundColor(){
    Color colorObject = getColorObjectFromString(myColorEvent.setColor(inputDisplays, colorOptions));
   background.setFill(colorObject);
  }

  private Color getColorObjectFromString(String newColor) {
    if(colorOptions.getString("Black").equals(newColor)){
     return Color.BLACK;
    }
    if(colorOptions.getString("Red").equals(newColor)){
      return Color.RED;
    }
    if(colorOptions.getString("Green").equals(newColor)){
      return Color.GREEN;
    }
    if(colorOptions.getString("Blue").equals(newColor)){
      return Color.BLUE;
    }
    if(colorOptions.getString("Yellow").equals(newColor)){
      return Color.YELLOW;
    }
    if(colorOptions.getString("White").equals(newColor)){
      return Color.WHITE;
    }
    return Color.BLACK;
  }

```
* Design: Pen Color and Background Color set up within the view. Both the set pen color and set background color features are buttons that are created using the ButtonFactory. They also use a ColorEventCall and a ChoiceDialogFactory to create a Choice Dialog with all the color options. The actual changing of the color however, is happening in the GameView class, as seen above in the setBackgroundColor() method and getColorObjectFromString() method.


* Evaluation: I think the implementation of the pen color and background color buttons could have been designed a little differently, as currently their inclusion in the abstract GameView class conflicts with the single responsibility principle. As mentioned earlier in the analysis, the view package was designed to have factories and event calls deal with creating Java FX elements and running their actions, while the abstract GameView class 
would just display the scene and all the changes that occur with it during the game. When my teammate implemented the pen color and background color change features, I feel that the factories and event call packages were not used enough. The method above getColorObjectFromString() gets the selected color and changes the pen color or background color to match this color. I think that this could have been added to the ColorEventCall() class so that the selected color could be processed immediately (since the color event call returns the string of the chosen color). I also think that instead of creating if statements, there could have been a map that had key value pairs for each selected string and its corresponding Color object. The setBackgroundColor() could also be within the ColorEventCall class, basically we would have this method within the class and have it call the setColor and getColorObjectFromString methods so that we could change the fill of the background node. I think this fix would keep functionality, but also allow for better consideration of the single responsibility principle. With these methods removed from the GameView class, I believe it would retain its responsibility of only displaying the view and its changes (not actually creating elements or running actions).




## Your Design

### Design Challenge
A design challenge I struggled with was how to deal with multiple GameViews for this project.

* Trade-offs

Some tradeoffs that came up while designing the separate GameView classes concerned how to switch games while playing one (for example going from Logo to LSystem). Since I wanted to create an abstract GameView that could be easily extended to create individual GameViews for each game, I was confused on how to allow for one game to switch to another. Basically, the  controller
class takes in an instance of the view as well as the model. I was struggling to figure out how I could change the instance of the view from game to game when a user tried to start a new game. When I tried to do this, I would get multiple null pointer exceptions as everything would be connected to the previous game's game view, and it was very difficult to initiate a new game view without basically initiating a new controller (which was only supposed to be done within the Main class).
I soon realized that with the abstraction for the GameView, it was difficult to change games, so I realized I needed to think of alternate designs.
* Alternate designs

Though I realized that I needed to fix my design, I still wanted to keep the abstractions for the view, so I thought of ways to initiate the GameView at the start of the game, and allow a user to restart the same game as much as needed. What I thought of here was to create a GameSelect class,
which would pop up a choice dialog at the start of the game with options to choose from Logo, LSystem, or Darwin to play. This choice dialog was created using a factory, and depending on which game was selected, the method setMyGameSelection() would initialize the respective GameView class with the selected game and language passed in (the language was also selected using a choice dialog). This view would then be passed into the controller, and the selected game string would also be sent to the controller for processing to decide which Managers and Animal types to use in relation to the game that was being played. With this alternative design, the only thing that would be reset once a user started a new game was that the same GameView would just be restarted again. So a user would choose Logo to play first, and every time they clicked New Game a new logo game would start.

* Solution

Since it was important to keep the abstractions so that the Open-Close principle would apply to the GameView set up, I decided to implement 
the game selection using the GameSelect class to select the game type at the start of the game, and allow the user to restart the same game multiple times. If the user wanted to start a different game they would need to restart the program by rerunning it. I implemented exactly what is described in the alternate design above.

* Justification or Suggestion

I am satisfied that the GameView follows Open-Close very well, as I was able to create LogoGameView, LSystemGameView, and DarwinGameView without having to touch the abstract GameView class at all. It is also extendable should a new game be added. I still do feel that I should have developed a solution that would allow users to switch into different games. I was recently looking back at the maze project, and I've been thinking I should have designed the GameView somewhat similar to that. Instead of having an abstract GameView class, I could simply have the general class, and still have LogoGameView, LSystemGameView, and DarwinGameView extend it. I would no longer need the GameSelect class, rather, the game could perhaps default start at Logo, but have buttons for each of the three games on the screen such that users could click them to start new (different) games. These buttons would trigger the respective GameView classes, and the scene would change as described within the extended classes. This would allow the passed in GameView parameter to be changed more easily in the controller, and it would give better functionality to the project. The most important part is that it would still follow the open-close principle. Thus, I think that this is something I would like to fix within my part of the project, but even so I am glad that the abstraction is well-designed currently.


### Code Design Examples

#### Good Example **you** implemented
```java
public class LogoGameView extends GameView{

    private ErrorPopUps myErrors = new ErrorPopUps();
    private ImageFileFactory myImage = new ImageFileFactory();

    /**
     * Constructor for LogoGameView. It has the same parameters passed in as GameView
     * @param myGame: Logo
     * @param myLanguage: selected Language to play in
     */
    public LogoGameView(String myGame, String myLanguage){
        super(myGame, myLanguage);
    }

    @Override
    protected Node makeVerticalPanel(){
        VBox right = (VBox) super.makeVerticalPanel();
        Button currentPosition = getMyButton().makeButton(getButtonDisplays(),"Position", event -> getTurtleLocation());
        Button penThickness = getMyButton().makeButton(getButtonDisplays(),"PenThickness", event -> changeStroke());
        Button changeLogoAnimal = getMyButton().makeButton(getButtonDisplays(),"ChangeAnimal", event -> changeAnimal());
        Label instructions = getMyTextField().makeTitle(getTitleDisplays(), "Logo");
        right.getChildren().addAll(currentPosition, penThickness, changeLogoAnimal, instructions);
        return right;
    }
```
* Design: Abstractions for GameView class + Factory and Event Call set up. The GameView class is an abstract class that sets up all of the panels within the scene, and updates the game scene as a game progresses. However, each game has its own unique features, so I developed extended classes such that each game is able to have the general functionality that GameView provides, as well as any added features unique to the game itself. When the GameView class creates an element it uses a factory class and links it to an event call. This abstraction and extension works assuming that a user has selected a game to play, as this is how the correct GameView is initialized. 


* Evaluation: I feel that I did a good job in making the view very extendable for multiple games to exist within the program. There are some features that every game needs, such as "New Game" or "Load Command File", but there are some that are only applicable to one , such as change pen thickness for Logo or play animation for Darwin. To combat this, I implemented the general GameView that sets up common functionality across each game, and any unique
features are added in the respective GameView. I think that having this abstraction is important in following the Open-Close principle, as with this I was able to add unique game features easily without ever needing to touch the abstract class. The program has the ability to hold multiple games as all that needs to be done on the view side of things is that a new GameView for a new game would need to be added. I think this is a good example of following the Open-Close principle. I also think that having separate factory classes allowed for consideration of the Single Responsibility Principle as the view never creates an element, it only displays it. In general I think that the GameView set up is a good example of both Single Responsibility and Open-Close, as every class does its own task, and everything can be extended. In terms of the abstract GameView class, I am happy with everything but lines 170 - 195. These lines were not added by me, and I feel that they could have been put into the already existing factory structure pretty well. This is something that was added toward the end of the project, at which point there was not enough time for me to make edits, and I also did not want to touch my partner's code without them being OK with it. I think if given just a bit more time I would have talked with my partner about changing this.
#### Needs Improvement Example **you** implemented
```java
 /**
     * Abstract method from GameView. For LSystem, the user is prompted to enter a list of parameters (length, angle,
     * numLevels) to be used within the game
     * @return comma-separated string of parameters
     */
    @Override
    public String setUpSystem() {
        return mySystem.createSystem(getTitleDisplays(), getInputDisplays().getString("LSystem"), getErrorDisplays().getString("LSystem"));
    }

```
```java
package oolala.view.factories;

import javafx.scene.control.TextInputDialog;
import oolala.view.events.ErrorPopUps;

import java.util.ResourceBundle;

public class SystemFactory {
    ErrorPopUps myErrors = new ErrorPopUps();

    /**
     * This method/class sets up the system for a game. It is primarily used by LSystem.
     * When a user starts the LSystem game, this method is called to ask the user to input
     * the length, width, and height for the LSystem game. Errors can happen here if nothing is inputted,
     * at which point the user is asked again to input parameters.
     * @param titleDisplays: This resource bundle contains all of the relevant labels (titles, error titles)
     * @param content: This is the string that is the key to the text to be displayed from the inputDisplay resource
     *               bundle. For example for LSystem, content: getInputDisplays().getString("LSystem"). Which
     *               maps to the prompt for the user to insert relevant LSystem parameters.
     * @param error: This is the error key that matches to a specific error in the ErrorTextDisplay ResourceBundles
     *             which is sent to the ErrorPopUps() class.
     * @return systemValues.getEditor().getText(): The string of parameters that the user inputted to set
     * up the game system.
     */
    public String createSystem(ResourceBundle titleDisplays, String content, String error){
        TextInputDialog systemValues = new TextInputDialog();
        systemValues.setContentText(content);
        systemValues.showAndWait();
        if(systemValues.getEditor().getText() == null){
            myErrors.showError(titleDisplays, error);
            createSystem(titleDisplays, content, error);
        }
        return systemValues.getEditor().getText();
        }
    }

```
* Design - LSystem parameters pop-up: If a user chooses to play the LSystem application, they are first prompted with a text input
dialog pop-up that asks them to insert the length, angle, and number of levels for the LSystem game. This is then passed to the controller, where the parseUserInput() method parses this input to find the three parameters and initiates the LSystemTranslator with these parameters such that the game will always draw at a certain length, angle and number of levels (until a new game is initiated).
This can fail if a user puts an invalid input, but it is caught and the application asks the user to restart the game in this case.


* Evaluation - On a functionality standpoint, this pop-up works well to initiate the LSystem game with the correct parameters, and these parameters are utilized for every drawing within the game. However as I was implementing the new Cell Society project and was thinking of ways for it to be extendable for any change that could be introduced, I realized that this part of my code is not very extendable. For example, the length, angle and number of levels can only be changed once per game, so if a user wants to change the parameters they must restart a new game. Also, if another game were to be added, we would have to make this a method within that game's respective GameView class, which is fine in terms of functionality, but would be repetitive in comparison to the setUpSystem() method in LSystemGameView. I think I would redo how I did this parameter set up. We know that the user must set these parameters before they can play a game, but I think these parameters should be easily changeable and should also be able to be used in other applications if needed. Thus, I would have made these buttons instead such that a user would click on a parameters button that would pop up the same dialog that currently shows up. The controller would then parse this in the same way. All we would really need to do is create an error handling mechanism that would detect if the user tried to run commands without setting the parameters. This way, the users could keep changing parameters within a game, and if another game needed this the parameters button could just be added to the abstract GameView class for extendability. I think these changes would follow the Open-Close method a lot better, and would be more functional as a result too.



## Conclusions

#### What part(s) of the code did you spend the most time on?
It did take time to actually write code and implement it, but the bulk of my time went toward thinking about design and how to make it both functional and extendable. At the start of the project my teammates and I spent almost ten hours just planning out step by step what each class would look like and how the model, view, and controller would interact. 
We created many use cases and mapped out a lot of pathways to better understand all the interactions. Then, when it came to doing my part (which was primarily the view and controller connections to the view), because I had a strong understanding of every aspect of the project, I knew how to connect things well to keep both functionality and extendability. In the end more hours would be attributed to writing code simply because there were many features involved, but I think while coding and before coding, simply thinking about the design was what I spent the most time on.

#### How has your perspective on and practice of coding changed?
I think I have changed a lot from the very first project to now. Looking back, I never really followed principles such as Single Responsibility and Open-Close very well. I had always made the effort to have clean code, but even that I think that I have improved on. Now, while coding, I always look for ways to split into multiple classes based on functionality and extendability. If I find one class is doing too many things, I look for ways to add classes that can take on specific tasks such that every class only has one responsibility. Whenever there is an opening to make a class abstract and extendable, I try to implement that because I know it will help in the future should anything new need to be added. I've really enjoyed this as well, I think before these projects I was very indifferent toward programming, but I feel more enthusiastic now and feel really proud of the work I do when I consider design choices.
#### What specific steps can you take to become a better designer?
At the start of class, I was worried that I would keep thinking of design as an afterthought. I would get full functionality first, and then go back and refactor, at which point the functionality would be affected at times. Now, I try to refactor as I go and think about ways to make everything extendable from the beginning. Be it in planning, or in coding, I think taking the steps to think about every line of code as you write it can really help in becoming a better
designer and also saving time from having to debug a lot. With every line of code, it is important that I confirm that the class I am in is following all the coding principles, and if ever I find one being violated, I can fix it right then. I've already noticed this in the Cell Society project, as I wrote a piece of code for the rules within the game, and then immediately realized I could better follow principles by extending it from a general class. So I think that prioritizing design just as much (and even more) than functionality when writing every line of code is important in becoming a better designer and programmer.