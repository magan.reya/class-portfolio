# Ethics Tech Takeover
### NAME: Reya Magan
### DATE: 11/15/2021


## World Domination Plans
The technological power I would use is to monitor health crises before they may occur.
Basically, I would want to have the power to detect (whether through biochips or some sort of health tracking)
issues that arise in someone that could possibly become fatal.
* what would you do with that power?

    
If I was able to detect that there was a problem in someone's body that could lead to a fatal health issue (for example I detect that an individual is about to have a heart attack, or abnormal cellular growth that could become cancerous is detected), I could send out an alert (either to the individual or in extreme cases emergency support) so that this issue can be addressed early, and survival chances can be increased.

* why, what are your goals?

My goal with this power is to improve life-expectancy in terms of medical issues. Cardiac arrest and cancer are both leading causes of death, and often this is because abnormalities in someone that can lead to both of these diseases are detected too late. If we can use technology to constantly monitor someone's health and survey for any concerns that could become fatal, we can find and fix these issues pretty early. This way, hopefully diseases like cancer or heart problems can become more avoidable and treatable as they are caught at the early stages. This would probably improve life-expectancy and make cardiac arrest/cancer lesser causes of death.

* what results do you expect or hope for?

My hope is that this technology would be able to quickly detect any possibly fatal issues that arise in an individual and promptly alert either them or medical personnel in the case of emergencies. If these detections are made as soon as the issue starts to develop, medical professionals would be able to treat these individuals much quicker (and before the disease has developed too much). This would likely lower the burden of many diseases on our global population.

* would you share this power? with whom and why?

This technology would just be running 24/7 in some data center, and anyone that subscribed to be a part of this technology would just have their health consistently monitored for issues that could occur. In this way, nobody else would really need to be involved, this technology would just be connected to local medical personnel and emergency services so that prompt medical support would be available. That being said, in terms of technological updates and ensuring the security and efficacy of the product, it may be good to have other technology professionals involved to continue to update the product.
These people would need to be vetted through an intensive process as this is very sensitive data that should be completely anonymous to the engineers and simply create a link between individuals and medical assistance. It is extremely important to maintain the privacy for all of the individuals using this technology, so any developer/engineer working on it would just update the product but not be able to see/use any sensitive data.

* would you want it known that you have this power or not? why or why not?

It would be important to have it known that this power exists, so anyone who wanted to use this technology would have the ability to do so. When used correctly and developed by the right people, I think this product could be really helpful in healthcare. I think it is important to take responsibility for this product because at its core it is for the well-being of any individual that uses it. Everyone involved in this product should take responsibility for it, and if ever anything goes wrong, should own up to mistakes and fix them. I think to develop trust and maintain an effective product it is best to have an open line of communication with the people that would use this product.

## World Domination Tech

* how might technology be used to give you this power or to distribute it?

With biotechnology on the rise, I think that this technology could be created in the form of a biochip that would just continue to monitor the health of the individual that has it. This would be an opt-in product: only people that want 24/7 surveillance of their health would have this product distributed to them. This is because it is not ethical to enforce a product like this as some individuals may want to keep their health information solely to themselves. For those that would like to be informed of any issues before they can arise, we would distribute this through them by having a trained professional attach the biochip. This most important thing with this technology is the trust and safety of the consumer, so every decision would be made with their consent and with the supervision of a trained health professional.
 