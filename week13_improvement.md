# Journal : Improvement Assessment
### NAME: Reya Magan
### DATE: 11/23/2021


## Habits

* Some to keep

    A habit that I was exposed to during this semester that I want to keep going forward is putting an emphasis on the planning stage of a project. Coming into this course, I would always just start coding and figure things out along the way. Now, there are times where it is helpful to just code things out, but more often than not I would become
    overwhelmed with so much code that I had no idea what to do with. I remember that I found the first few individual projects quite difficult because I just jumped into them without  really knowing what I wanted to accomplish. When working on OOLALA, my partners and I spent at least 8 hours just on planning. At the time, I wondered if this was really necessary since we could have just started coding and figured it out along the way, but looking back,
    I am extremely glad we spent so long on the planning. On the OOLALA project, even though I was on the frontend I knew everything about the inner workings of the backend and the project as a whole - I was even able to jump into the backend when issues came up. In comparison, my team in Cell Society though great did not spend much time on planning. This ended up being very stressful later on because I knew a lot about the backend, but felt quite lost as to
    what was happening on the frontend. I think going forward on projects, even if I take on a specific role, I would like to understand everyone else's roles as well. I think that planning is crucial to all projects, not just programming projects. Spending a decent amount of time on planning is a habit I definitely want to keep going forward.

    Another habit that I find very important to keep is communication. I was on teams where everyone was good about communicating, and also on teams where people were quite unresponsive. In teams where people were not very responsive, I still made sure to update everyone on my work. Eventually, people also started responding. At the time I worried that perhaps I was being pushy or would come across as somewhat overbearing, but I think that communication is extremely
    important to the success of any team project. So I think going forward I am okay if sometimes I may need to send many messages/updates before a team starts to respond, because it is more important that the team keeps up a communication and succeeds.

* Some not worth keeping
    
    One thing that I worked on during this course and am still working on is when to refactor/design code well. At the start of the semester, I used to just design the entire program and then go back and refactor. Now, I did not know much about design principles then, but it definitely was quite painful to go through so much refactoring all together. In OOLALA I also refactored a lot at the end, which ended up being quite stressful. Going into the final two projects, I really tried to refactor as I coded. There were instances where I developed functionality first and went back refactored later, but I tried to not leave too much refactoring to be done. That being said, I do admit that there are times where I have the mindset to get it to work first and then refactor. This works to an extent, but gets really stressful after a time. So this is something that I have been working toward and will continue to develop so that I always refactor as I code.

* Some to explore later
    
    I think that I would like to get into the habit of working on my own individuals projects. Throughout this semester I have learned to write full programs with little to no started code, and as a result I have developed more confidence in my programming ability. I think I would like to continue to explore this growth by working on individual projects whenever I have the time. 

* Value of checklists?
    
    I think checklists are pretty useful in organizing the thought process behind programming. I used an actual checklist for the first few projects, but after that started to keep a sort of running checklist in my mind of design considerations that needed to be made and checked. I definitely think having a checklist when I was being introduced to design decisions for the first time was really helpful, as it helped me to keep my thoughts organized and to stay on top of my work. As I learned more about design, this checklist just became a habit of mine - so much so that I no longer needed to physically check things off.
    I think thus, that when it comes to learning new things, checklists are really important for getting into a routine, and once that routine is developed, this checklist just becomes a part of your regular thought process.


## Programming Skills

* Delightful code

  ```java
  return (Command) Class.forName(
  String.format("oolala.commands.%s", myInstructionSet.getString(currentIsn)))
  .getConstructor().newInstance();
  ```
  At first glance, this is just a simple line of reflection code, but this was quite inspiring to me. This code was written by one of my partners for the OOLALA project. I remember our team being worried that we had too many if-statements to set up our commands for the OOLALA game, and we couldn't figure out a good way to refactor it. I asked my TA about this and she mentioned that since we hadn't learned about the best way to refactor if statements it was okay to keep it as is. I took this information and moved onto a separate part of the project, but my partner went and found out about reflection and learned how to implement it from her TA. I was really impressed by how passionate she was about learning new ways to design code better. After this experience, I started to become more active in my coding process - using new tools to make my code better whenever I could. So I think that this simple line was really delightful to me, and it pushed me to also become more passionate about coding.

* Lessons from worst coding experience
  
  My worst coding experience was the last few hours of the CellSociety project. I had no idea what my teammates had been working on for the last few days as the branches were not updated, and there was no communication in the group chat. I had tried to reach out, but after no response I felt that maybe I was being too pushy, so I started to implement parts of the project on my own. Towards the last few hours, more code started getting pushed to master, and the whole program crashed. This was extremely stressful for me, as it felt like two weeks of hard work had gone down the drain. One of my partners and I were eventually able to fix this, but this was an experience that I never want to go through again. I think that this happened because of poor communication between the frontend and backend teams. I learned after this night that there is no point in worrying about being too "pushy" when it comes to team projects, because communication is extremely important to ensure that everyone is on the same page and last minute crashes like this don't happen.

* Lessons from best coding experience

  My best coding experience is pretty much the opposite of my worst coding experience. I would say that my best coding experience was the OOLALA project. My partners and I were really focused on planning, and made it a point to keep each other in constant communication. As a result, I felt confident about the whole project (not just my part), so whenever anyone needed I was able to step in to help. I learned truly how important communication is through the combination of this project and CellSociety. Just being on the same page as everyone else on the team reduces so much stress, and I found the OOLALA project to be much less stressful because of this. My team for OOGA has been doing a great job with communication too, and overall I have felt much more calm about this project as well.

* Interesting skills beyond coding

  This is still somewhat related to coding, but I think I have developed a passion for learning about code and about ways to design it better. I know that this is the goal of the course itself, but truthfully coming into this class I had very little confidence in myself as a programmer. I have gone through classes and felt like I was in the wrong major, but after taking this class my confidence has really grown a lot. I have realized that I can be a good programmer, and I have started to really enjoy programming so much so that I want to do it as a full-time career (and not stick to my original plans of going down an electrical engineering track). So I think the biggest skill I have gained from this course is passion and confidence, so I am extremely glad that I took this course because for the first time I feel secure in my decision to do computer science.


## Personal

* Personal strengths

  A strength that I feel I have is that I am good at being communicative and collaborative within teams. I have done multiple team projects in the past, and have felt that I work well with others and am a good team player. I think this course challenged this strength, not in the sense that I became less good at working in teams, but in the sense that I was exposed to a variety of different scenarios that required different methods of teamwork. I worked with people who were great partners and team players, and some that were great individuals, but not the most responsive partners. I feel that as a result I have developed a more dynamic strength of working well in teams, because I have worked with very different teams and individuals throughout this semester. I now feel confident that I can work well with teams that have many different backgrounds and behaviors, so long as I keep communication flowing within the team. I think this will really help me going forward in my career.

* Personal weaknesses

  One weakness I had was my anxiety and lack of confidence when it came to coding, this has largely improved as a result of this class as I have taken on challenges and pushed myself to be better and work harder. In terms of coding, I used to just write thousands of lines of code in one file and have it be the whole program. I have come a long way since then, and have written much more structured and modular code. I also pay a lot of attention to the design of the code as I write it, and not as an after-thought. I think in terms of designing code I have made a lot of progress, but there is always more room to improve, so this is a skill I want to continue to practice.

* Habits to stop

  Same as above, the biggest habit I had was coding and then doing large amounts of refactoring at the end. I do not not do this nearly as much any more (though there are times that I can get lost in the weeds of functionality), and as a result feel that I have a better understanding of code and how it works. I still have a ways to go before I become a great programmer, but I think stopping this habit has made me improve a lot.

* Personal development steps

  As I finish up this course and my senior year at Duke, I have a better understanding of where I want to be in terms of my career. I definitely want to be involved with product development and programming, and would eventually like to have a manager role. I think continuing to hone my technical skills by taking on individual projects will be really helpful in my development as a programmer. Once I start working after graduating, I really want to focus on building connections and relationships through strong teamwork and communication. I think improving my technical skills while also being a great team player will help me to reach my career goals.