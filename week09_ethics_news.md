# Journal : Ethical Essay
### NAME: Reya Magan
### DATE: 10/25/2021


## Issue
The ethical issue that I chose to look at concerns self-driving cars, specifically moral dilemmas they can 
face such as decision-making during pedestrian/other car interactions. Just like regular cars, self-driving cars can
face sudden situations in which collisions may happen. There have been multiple arguments on the safety of self-driving cars
when it comes to situations like this, with some arguing that self-driven cars are actually safer than regular cars during these situations,
and some believing that the pre-programmed nature of these vehicles is extremely inhumane.

## Perspective 1
The first perspective I want to discuss is of the people who believe that self-driven cars are safer in collision/accident situations than human-driven cars. 
They back their claims by using statistical data, as reports by McKinsey and Company have shown that self-driving cars actually reduce accidents by almost 90%. They argue
that the pre-programmed nature of self-driven cars is actually a good thing, because the cars will always follow traffic laws and well never text or drink while driving. There also isn't a concern
for impaired visuals within self-driving cars - they can see everything around them (front, side, or back!). They also argue that these cars are likely more moral than some humans are, as a survey of 2.3 million
people around the world revealed that some individuals from certain environments with stricter laws tend to be fairly unforgiving to pedestrians that are on a road at the wrong time. The supporters of self-driving cars argue that 
if individuals are making such extreme decisions, more accidents can happen versus a car being programmed to follow all traffic laws and guidelines. The autonomous cars would be programmed in a utilitarian manner - such that decisions
would be made for the benefit of the majority of individuals involved. In the perspective of the supporters, even though autonomous cars do not eliminate the occurrence of accidents, they make the safest decisions for the individuals involved within them - while human accidents
can become complicated due to sudden emotions.


## Perspective 2
On the flip side, there are individuals who argue
that the predetermined nature of autonomous cars is extremely
unethical, since these cars are have already "decided" what will happen
to individuals that may be involved in an accident. These people believe that it is
better for vehicles to operate in a natural manner - accidents will happen, but a machine 
won't be "controlling" their outcomes. These individuals also consider having self-driving cars
have the option to give complete control back to the drivers. In this case, if an accident does happen,
it would not have a pre-determined result necessarily. This way, an individual's own moral compass will dictate
their actions, and engineers/government/policy makers will not have any say in this matter.
## Personal Analysis
I think that there is a valid concern surrounding autonomous cars and the safety threats that they may pose to humans.
In my opinion as well, it does seem somewhat uncomfortable and questionable that a vehicle operates on a pre-determined 
set of instructions that it was trained on - and that drivers have little to no say in that. However, I still think autonomous
cars are a good technological advancement. I think that the fear that some people (including myself) have has a lot to do with media
over-exaggeration of the negatives of autonomous driving. I think there are more positives to self-driving cars than negatives.
First, it has been shown that objectively autonomous cars did actually reduce accidents. Going off the Mckinsey report, if autonomous cars
continue to prevent 90% of accidents, 30,000 lives in the US can be saved per year. Additionally, the pre-determined nature of these cars is not something
that is unethical - these decisions are made by a talented team of engineers and scientists - and all such decisions are vetted by everyone involved.
A lot of these autonomous vehicles are trained using Machine Learning mechanisms that are guided on utilitarian principles. Basically, the cars will always make the 
decision that benefits the majority of people involved. It seems to me that in the long run, accidents and collisions would be reduced by a good amount with more and more 
autonomous cars, but I think they need to be very rigorously tested and implemented before a lot of people (including myself) would be fully comfortable and receptive to them.


## Impact
I think ethics within technology is really important, in fact I spent a semester working on a Bass Connections Project to introduce ethics
into the first-year engineering curriculum at Duke. Throughout this process I learned a lot about different ethical ideologies, one of them being 
utilitarianism. I think there is no perfect answer to ethical concerns regarding technology. We can try as hard as we want, but no advancement can have a 100% positive outcome. I do think
that having a strong ethical framework in mind, perhaps by using principles like utilitarianism will help to make ethically sound tradeoffs when designing new technology.
To this end, I think I would want to work at a company that uses some sort of ethical framework to vet the decisions that they make. If I was ever to work in the autonomous vehicle industry, I would
definitely work for a company that emphasizes ethical concerns extremely highly within all of their design considerations. Wherever I work, it is really important to me that the 
technology that I design/work on has a positive impact on the majority.

## Sources

* Source 1

https://www.nature.com/articles/d41586-018-07135-0

* Source 2

https://towardsdatascience.com/the-ethics-of-self-driving-cars-efaaaaf9e320

* Source 3

https://www.nae.edu/221212/Why-Everyone-Has-It-Wrong-about-the-Ethics-of-Autonomous-Vehicles
