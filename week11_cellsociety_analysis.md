# Journal : Cell Society Project Analysis
## Name: Reya Magan
## Team: 9
## Date: 11/7/2021


Design Review
=======

### Adding new Automata
If someone wanted to add a new automata application to this project, it would be fairly easy to do so.
These are the steps that they would need to follow:
1. Data Files: For the new game, we would need new data files (.sim, .csv) that can be loaded and ran within the application.
   The data folder already has GameOfLife, Fire, Percolation, SchellingSegregation, and WaTor sim and csv files, so we would
   just need to add new (sim, csv) pairs for the new automata game. In the .sim file, we would need to have the game type, the author,
   the title of the game, its description, and then any other optional parameters regarding the specific game (CellShapes, 
   StateColors, Neighbors etc.). Some games require variable parameters (such as the percent of neighbors that need to be the same as the current cell within Schelling
 Segregation), so this would also be described in the sim file (eg. percent = 50). The CSV files would describe the number of rows and columns to be displayed in the grid, as well as each of
   the state values at each cell. These cell state values could either be defined by the actual values for the game (for example 0 = DEAD, 1 = ALIVE),
   or they could be described as unknown "x" values, which would then be randomly generated given the knowledge of valid states for the current automata game.
2. Rules: After setting up the data files, the next thing that we would need to do is create the rules for this game. We already have an abstract Rules class that has 
three extended rule classes: RuleEqual, RuleGreaterThan, and RuleLessThan. This class was made because each of the already existing automata games can be minimized to equal to, greater than, and less than rules.
For example, in the SchellingSegregation game, if the percent of same group neighbors is less than the threshold of neighbors, the cell needs to move. On the other hand, if the percent of same group
neighbors is the same as or greater than the percent threshold, the cell can stay where it is. All of this decision-making about a cell's next state is done by these three greater than, equal to, or less than rules. What this means is that for any new automata game,
we could likely minimize it down to these three rule types as well. That being said, if this new game requires consideration of something else than the greater than, equal to, or less than rules, we would be able to easily add this new class extended from the abstract rules class.
3. Rules Structure: The next thing that we would need to do is actually set up how the new automata uses our Rules. This is all done through property resource files. We have a CellSocietyRules class that uses reflection to run the correct rules for the current game. These rules are read from the game's respective
property file. An example is as follows for the GameOfLife:
```java
   RULE1=< 2 DEAD
   RULE2=eq 3 ALIVE
   RULE3=> 3 DEAD
```
This property file uses reflection to call the Rules classes defined above (eg. > = GreaterThan). We have a TranslationBundle that maps each of these inequality symbols to the correct class name. In the case of GameOfLife, we know
that if the number of ALIVE neighbors is less than 2 or more than 3, the cell's next state will be DEAD. Then, if the cell
has 2 or 3 ALIVE neighbors, it will also be ALIVE. This behavior would be similar for the new automata as well. Depending on what conditions the neighbors of the cell needs to satisfy, this property file will have 
the implemented. For example, if we assume that this game has two states: Orange and Blue, and if the cell has 4 of its same group neighbors it becomes Orange, whereas if it has less than 4 or greater than 4 of the same group neighbors it becomes Blue, then
the property file would look like this:
```java
RULE1 = < 4 BLUE
RULE2 = eq 4 ORANGE
RULE3 = > 4 BLUE
```
This means that if the cell's current neighbors is less than 4, the cell's new state will be Blue (the other two lines follow the same explanation just with different inequalities).

We would create an extended class for this new game that calls super() to our abstract class, and make sure that this Resource property file has the same name as the extended Rule structure class (eg. "GameRules.properties", "GameRules.java"). We would then be set to implement our rules for the new game,
as the abstract class would call this correct resource file and run through its rules as defined above. 

However, in some cases the number of neighbors that have to be a certain state is variable - for example in SchellingSegregation, the number of neighbors that has to be the same state as the current cell varies game by game depending on how it is defined in the sim file. If our new automata game is in this format, our property file for its rules would be similar but would need one difference. If we go back to the orange and blue example above, but this time instead of 4 neighbors being the same state we define this number in our sim file our property file would look as follows:
```java
RULE1 = < neighborCheck BLUE
RULE2 = eq neighborCheck ORANGE
RULE3 = > neighborCheck BLUE
```
This "neighborCheck" parameter would actually be a method defined in the class for this new game's rules (that is extended from the abstract CellSocietyRules class). We would use method-reflection to call this method, get the relevant parameter from the already parsed sim file parameters (our GameFactory class parses the sim file and returns all of the parameters into a map that is passed through the relevant classes), and return this as the number to be considered in the greater than, equal to, and less than classes. We do not have to implement anything much for this as the SchellingSegregation and Fire games already use this method-reflection. We would just need to create the property file, and then create the "neighborCheck" method within the new game's extended class to return the number of neighbors that need to be a certain state as defined by the game's sim file.


4. Model: The model has a few classes that may need to be updated. They are as follows:
   1. Resource State Bundle: We define all of the states for the current game within the respective Rule bundles. An example for GameOfLife is below:
        ```java
        ALIVE=1
        DEAD=0
       ```
         We would make a new resource property file for this new automata game, that would just map state strings to their integer values just as they have been defined for the already existing games. These states would match those found in the CSV files as well. In the case the CSV file would have a state that was not defined within the Model, an error would be displayed to the user that this file is invalid.
   
         If this new game is defined similarly to SchellingSegregation or WaTor (in which the cells do not change state, rather they "move" to new cells), the property file would look similar to the one below (for WaTor):
       ```java 
      EMPTY=0
      FISH=1
      SHARK=2
      SAME=3
      MOVE=4
       ```
      In this case the csv file would just have states 0,1, or 2. However, when the current cell would be passed to the rules class, either the "SAME" or "MOVE" state would be returned, and this would signal the model to move the cell, and then set its state to be either Empty, Fish, or Shark.
      Depending on the type of game this new game is, we would make one of the above two resource files to define the cell states.
   2. CellSocietyModel: After the resource files, the first thing we would want to update is the CellSocietyModel itself. We have our CellSocietyModel defined as an abstract class, and all of the different games have their own model that extends this abstract model. What we would need to do is create a new class for the new automata game's model, which would extend the abstract Cell Society class. The abstract class already initiates the correct Rules class for the current game using reflection (it takes in the game type an initializes a CellSocietyRules object matching the game type, so for SchellingSegregation it would set the rules to be SchellingSegregationRules), so all we would need to do 
   is make sure that we have that Rule resource bundle already created as well as the corresponding game Rules class. Then depending on the type of the game, we would have the following ways to structure our extended model class:
   
        If the game was similar to GameOfLife, Fire, or Percolation in that a cell's own state would just change depending on what the rules classes return, then our model class would be really simple. It would follow the following structure (taken from GameOfLife):
   ```java
     @Override
     public void setNextState(Cells myCell, int row, int col, Grid myGrid) {
     int initialState = bundleToInteger(ALIVE);
     int quantityOfLivingCells = quantityOfCellsOfGivenStateInCluster(initialState,
     neighborGenerator(row, col, myGrid));

    Integer alive = bundleToInteger(ALIVE);
    Integer dead = bundleToInteger(DEAD);

    Map<Integer, Consumer<Integer>> intMap = Map.of(alive, integers -> myCell.setMyNextState(
            getMyRules().generateNextState(quantityOfLivingCells, myCell.getCurrentState())),
        dead, integers -> myCell.setMyNextState(
            getMyRules().generateNextState(quantityOfLivingCells, myCell.getCurrentState()))
    );
    this.consumerGenerateNextState(myCell.getCurrentState(), intMap.get(myCell.getCurrentState()));
    }
    ```
   We would just override the setNextState method which exists in the CellSocietyModel abstract class, to take in the current state of the cell (in this case ALIVE or DEAD), and then use a consumer to call the rules class and return the correct next state of the cell. We would then set the next state of the cell to be this returned value, as can be seen in the consumer map above. For our new game we would just implement its own version of this method depending on its possible states and rules.

    However, if the new game involves cell movement - meaning that the cell does not just change state into something else, rather it finds an empty cell and moves to that - we would need to implement our model a little differently. We would do something similar to the SchellingSegregationModel class below:
   ```java
    @Override
    public void setNextState(Cells myCell, int row, int col, Grid myGrid) {
    List<Cells> myNeighbors = neighborGenerator(row, col, myGrid);
    int numSameCells = quantityOfCellsOfGivenStateInCluster(myCell.getCurrentState(),
    neighborGenerator(row, col, myGrid));
    double propSameCells = percentSameNeighbors(numSameCells, myNeighbors);
    int state = getMyRules().generateNextState((int) (
    SCALE_FACTOR * propSameCells), myCell.getCurrentState());
    mySchellingSegregationMovement.setInitialParameters(myCell, myGrid, myNeighbors,
    getStatesBundle(), getMyParameters());
    mySchellingSegregationMovement.checkState(myCell, state);
    }

    private double percentSameNeighbors(int sameCells, List<Cells> neighbors) {
    neighbors.removeIf(c -> c.getCurrentState() == bundleToInteger(EMPTY));
    int neighborsLeft = neighbors.size();
    return ((double) sameCells / neighborsLeft);
    }
   ```
   We would still override the setNextState method from the CellSocietyModel, but instead of using a consumer to update the current cell's next state, we would use a CellMovementInterface to determine this. The reason for this is that the rules class for these movement games will return either MOVE or SAME depending on where the current cell should go next. We would then need a movement interface to determine what these MOVE or SAME states mean in the context of the game being played. This interface would then return the correct next state for the cell (for example, if a SHARK cell in WaTor returned a MOVE state from the rules, the interface would "move" this cell to a FISH or EMPTY cell, and then return EMPTY for the recently moved SHARK cell)
   3. CellSocietyMovement: The cell society movement interface would only need to be updated if the new game is similar to SchellingSegregation and WaTor in the sense that it requires cells to "move" to a new cell within the grid. This would be fairly simple to implement. We already have the interface that implements the methods checkState, keepState, and moveState. The two games that already use this interface implement it differently in their own CellMovement classes. For this new game, we would just do the same, in that we would implement the CellMovement interface methods in the way that is relevant to the new game. For example, if the current cell is ORANGE and it returns a MOVE state from the model/rules, then the movement class would first check this returned state, see that it is a MOVE state, and then call the moveState method (using a consumer) to move the ORANGE cell as described in the rules of the game.
   4. Neighbors: The neighbors class has different types of neighboring policies that a game can implement. The neighboring policy is defined in the sim file for the game being played. We have an abstract CellSocietyNeighbors class, and then extended neighbor classes that implement the correct neighbor policy (FullNeighbors, LeftRightNeighbors, etc.). If this new game has a neighboring policy defined in the sim file that does not already exist, it would be very easy to implement. We would simply create a new extended class and override the abstract generateNeighbors() method from the abstract class to follow this new neighboring mechanism. 
5. Controller: The controller would not need any updating, all it is doing is getting a file from the view, parsing it, and then using reflection to call the correct game model depending on the selected game. All of the classes within the Controller (Grid, GridFactory, and GameFactory) would operate in the same way because they have the ability to parse files as they are given - so even if the new files have more parameters than the other game files, the classes can still parse them completely and the controller can initialize the correct model class.
6. View: The view would not need much updating. The classes that do need updating are below:
   1. Property files: We would need to update the English and Spanish properties file to have the new game and its title so that this can be displayed on the game view when the new game is loaded.
   2. CellColors: We would need to update the CellColors class to have this new game, and its default state colors defined as well. These state colors would be used in this new game unless the sim file had its own unique colors defined.
   3. CellView: The CellView class allows us to have the cells within the grid be multiple shapes (Hexagon, Square, or Triangle) as defined in the game's sim file. If a new shape is introduced, we would simply need to create its own method (eg drawTrapezoid()) just like the drawHexagon(), drawTriangle(), and drawSquare() methods that already exist. 
   
All of the features for this new game could be added without really having to touch much existing code at all, we would really just need to make more extended classes within the abstractions we already have.

### Overall design
For this project, we used the MVC method to connect our frontend to the backend via a controller to keep track of communication. We designed the code such that everything starts after a user uploads a sim and csv file for a game, and then selects the "Play Game" button to start the game. The controller parses these files, and determines which game is being played and what the initial states of the grid are for that game. This information is then used to initialize the appropriate model class for the game using reflection (we read in the game type string, and then call the model class with the corresponding name). Once the "Play Game" button is pressed, the view calls the step() method in the controller every 2 seconds. This step() method takes every cell in a grid and goes into the model to determine its next state dependent on the states of its neighbors. The cell's next state is determined through the defined rules of the game, which are found in the Rules and ruleStructures packages. Once every cell has been updated to know its next state, the controller calls the updateGrid() method (line 133). Within the updateGrid() method, each cell's next state is set as its current state. This all happens through the Cells class, as each cell within a grid is a unique Cells object - that has knowledge about its current and next cells. This process continues indefinitely until a new game is selected to be played. To make this more clear, we can take an example of the GameOfLife game.

Let us assume that we have chosen to play the GameOfLife game. The interconnected process is as follows:

1. At the start: The user will select the "Upload Simulation File" and select the game_of_life folder, and for the purpose of this example will select the "blinkers.sim" file.
2. In the Controller: This file is immediately sent to the controller into the loadFileType() method (line 49). This method will look at the file to find its type (either .sim or .csv). In this case, the file is a .sim file, so the GameFactory class will be called to parse this sim file.
3. Within the GameFactory class: The setUpModel() method (line 33) will parse the sim file, and create a map of all the parameters where the keys are the strings before the equal sign in the sim file, and their values are after. For example, for the line "StateColors = #F0000, #DDD67" within an imaginary sim file, the key in the map would be "StateColors" and the value would be "#F0000, #DDD67". This method also returns the value of the game type (which is also in the map of all the parameters)
4. Back in the Controller: The controller calls the getParametersMap() method from the GameFactory to return all of the parameters within the sim file. Now that the controller has the knowledge of the game type and all of the relevant parameters, this is used to initialize the correct model class. The controller uses the game type string to use reflection to initialize the correct model for the game (in this case GameOfLifeModel), and the map of parameters is passed into this model.
5. CSV File: After the sim file has been taken care of, the user will select "Upload CSV File" and select the game_of_life folder, and select "blinkers.csv". 
6. In the Controller: This file is immediately sent to the controller into the loadFileType() method (line 49). This method will look at the file to find its type (either .sim or .csv). In this case, the file is a .csv file, so the GridFactory class will be called to parse this csv file.
7. Within the GridFactory class: The setUpGrid method (line 55) will parse the csv files, and call the Grid class to initialize all of the cells within the grid. This Grid object will be returned to the controller.
8. Play Game: After the csv and sim files are loaded, the user can then click the "Play Game" button to start the simulation (NOTE: if this button is clicked before both files are loaded this will return an error pop-up asking the user to load the proper files). Once the "Play Game" button is clicked, the startGame() method in the CellSocietyView class (line 124) is called using method reflection. This method will start the animation, and begin calling the step() method in the controller via the step() method in CellSocietyView (line 183).
9. Step() in the Controller: The step() method in the controller will be called every two seconds, it iterates through the Grid object to get each cell at its row, col position. It then passes this cell into the setNextState() method in the model. In this case we would go into the GameOfLifeModel class.
10. In the GameOfLifeModel class: We are now in the setNextState() method within the GameOfLifeModel class (line 43). This method will first take the cells row, col position and pass this into the Neighbors class, where a list of valid neighbors of the cell will be returned. In the case of blinkers the neighbor configuration is FullNeighbors according to its sim file, so all of its surrounding neighbors are considered. Once the list of neighbors that are ALIVE are returned, this number along with the current state of the cell is considered within a consumer set-up. Given the current state and the number of ALIVE neighbors, the rules class is called to determine what the next state of the current cell should be. This happens on lines 51 - 56 within the GameOfLifeModel. The GameOfLifeRules class is called.
11. Within the GameOfLifeRules class: We are now in the GameOfLifeRules() class, which has all the functionality of the abstract CellSocietyRules() class. So if we look at the CellSocietyRules() class, we see that the relevant resource bundle is instantiated right when the cell is called - meaning we are using the GameOfLifeRules.properties file to run through the rules. Then, the generateNextState() method (line 95 in CellSocietyRules.java) is called, which calls the generatedStateRunThroughRules() method to run through the relevant GameOfLife rules, which are below:
      ```java
       RULE1=< 2 DEAD
       RULE2=eq 3 ALIVE
       RULE3=> 3 DEAD
    ```
    These call the relevant classes within the Rules package (RuleLessThan.java, RuleEqualTo.java, and RuleGreaterThan.java respectively). Depending on which of these above conditions is met, this is returned back into the model, where the next state of the cell is updated. So if the number of ALIVE neighbors for a cell is 1, the rules would return to the model that the cell's next state should be DEAD, and the model would update this. This happens for every cell within the grid.
12. Once each cell's next state is updated: After each cell has its next state set, the updateGrid() method within the controller (line 133) is called, and each cell's next state is set as its current state. This process will happen every time the step() method is completed (and step() in the controller is called every 2 seconds).
13. Back in the view: While the step() method is being called in the controller and each cell in the grid's next states are being updated, the view also reflects this on the display. Each time step() is called in the view, the setupGridPanel() method is also called, which calls the setUpGridSection() method (line 340), this updates the GridView (using the GridView class) to be the new grid, which is taken from the controller's getMyGrid() method (line 69 in the controller). The controller always has the most updated version of the grid, so the view will display this accordingly.

The above 13 steps illustrate a basic integration of all of the classes and methods within the application, connecting the frontend and backend via the controller. There are more buttons such as "Speed Up" and "Slow Down", but these are only relevant to the view. Any view interactions will communicate with the backend using the above steps, and in the case of clicking to change a cell's state, the cell class will also be updated to reflect this. So as we can see, just a few button clicks can accomplish many tasks within our design. It is easy to load a sim and csv file, start the game, and then just view the simulation as it continues.

### Design Principles

#### Liskov Substitution

* Good or __Needs Improvement__
```java
@Override
//GAME OF LIFE MODEL
  public void setNextState(Cells myCell, int row, int col, Grid myGrid) {
    int initialState = bundleToInteger(ALIVE);
    int quantityOfLivingCells = quantityOfCellsOfGivenStateInCluster(initialState,
        neighborGenerator(row, col, myGrid));

    Integer alive = bundleToInteger(ALIVE);
    Integer dead = bundleToInteger(DEAD);

    Map<Integer, Consumer<Integer>> intMap = Map.of(alive, integers -> myCell.setMyNextState(
            getMyRules().generateNextState(quantityOfLivingCells, myCell.getCurrentState())),
        dead, integers -> myCell.setMyNextState(
            getMyRules().generateNextState(quantityOfLivingCells, myCell.getCurrentState()))
    );
    this.consumerGenerateNextState(myCell.getCurrentState(), intMap.get(myCell.getCurrentState()));
  }
```
```java
@Override
//SCHELLING SEGREGATION MODEL
  public void setNextState(Cells myCell, int row, int col, Grid myGrid) {
    List<Cells> myNeighbors = neighborGenerator(row, col, myGrid);
    int numSameCells = quantityOfCellsOfGivenStateInCluster(myCell.getCurrentState(),
        neighborGenerator(row, col, myGrid));
    double propSameCells = percentSameNeighbors(numSameCells, myNeighbors);
    int state = getMyRules().generateNextState((int) (
        SCALE_FACTOR * propSameCells), myCell.getCurrentState());
    mySchellingSegregationMovement.setInitialParameters(myCell, myGrid, myNeighbors,
        getStatesBundle(), getMyParameters());
    mySchellingSegregationMovement.checkState(myCell, state);
  }
```
The Liskov Substitution Principle basically states that we should be able to substitute a call to a super class with any of its subclasses and the program should not break or behave unexpectedly. An example to look at is the inheritance hierarchy for the CellSocietyModel. We have an abstract CellSocietyModel class, and each game type extends this class. The only method that needs to be different for each of the extended classes is the setNextState() method, which determines the next state of a cell given the current game's rules. Above we have an example of the GameOfLifeModel and the SchellingSegregationModel classes' implementations of setNextState(). These partially follow the Liskov-Substitution principle in that if we were to replace the call to CellSocietyModel that uses reflection to run SchellingSegregationModel to instead run GameOfLifeModel the program would not break. Of course it would not return correct next states for cells - but this makes sense because we are implementing rules for one game into another game. That being said the program would not break. 
However, if we implemented SchellingSegregation instead of CellSocietyModel the game could break. As we can see above, in the GameOfLifeModel the consumerGenerateNextState method is called, but it is not called in the SchellingSegregationModel. This means that the consumerGenerateNextState() method within the super class CellSocietyModel is not something that is used by all of the classes that extend it. This can cause the program to break if the super class is replaced with a class that does not use the consumerGenerateNextState() method, like the SchellingSegregationModel class. If we substituted the SchellingSegregationModel class to be the super class, then games such as GameOfLife would crash because they would be calling this consumerGenerateNextState() method that does not exist in the "super class". Thus, instead of having this method in the CellSocietyModel and having some of the game models call it, we could extract this method into an interface and have the games that need to call this method implement that interface. That way, even if a game that does not implement that method were to be substituted in place of the CellSocietyModel superclass, the program would not crash because the consumerGenerateNextState() method would be implemented within an independent interface, and this inheritance hierarchy would only have methods that are the used across each game type.
#### Dependency Inversion

* Good or __Needs Improvement__

Dependency Inversion means that a high-level module should not rely on its low-level modules. Meaning, an abstract high-level class should not be affected by changes in the low-level classes that extend it. This is implemented well in the CellSocietyRules and Rules classes. First of all, CellSocietyRules is an abstract class, which is extended by GameOfLifeRules, FireRules, etc. CellSocietyRules for the most part does not rely on any of these classes, rather they utilize all of the methods that
are defined within CellSocietyRules. One issue that could be fixed however, is that the FireRules and SchellingSegregationRules implement methods called probabilityCheck() (line 23 FireRules) and percentCheck() (line 22 SchellingSegregationRules). These methods are called using method-reflection as seen below:
```java
protected void initializeMyRules(){
    translationBundle = initializeBundle(ruleResourceBundleBase, "TranslationRules");
    for (String eachKey : ruleBundle.keySet()){
      String ruleString = ruleBundle.getString(eachKey);
      String[]ruleSet = ruleString.split(" ");
      if(eachKey.contains(METHOD_RULE)){
        try{
          Method method = this.getClass().getDeclaredMethod(ruleSet[1]);
          method.invoke(this);
        }
        catch (Exception e){
          myErrorFactory.updateError(RULE_ERROR);
        }
        ruleSet[1] = parameter;
      }
      runThroughRules(ruleSet);
    }
  }
```
The relevant parameters are returned after either the probabilityCheck() or percentCheck() methods are invoked in the extended subclasses. In this case then, the high-level class does care about details within the lower level class (as it is invoking methods from the subclass), which does not follow Dependency-Inversion. This could be fixed by having two different types of interfaces for either pre-set parameters (and this interface would just return them) or user-defined parameters (and this interface would read from a property file and return them). CellSocietyRules could implement these interfaces, and the extended classes would implement the interfaces relevant to themselves, then the parameter checking within the abstract class would not depend on its extended classes. Rather it would be something completely independent dealt in interfaces, and just taken from there.


#### Interface Segregation

* __Good__ or Needs Improvement
```java
public interface CellSocietyMovement {

  void setInitialParameters(Cells cell, Grid grid, List<Cells> neighbors, ResourceBundle statesBundle,
      Map<String, String> parameters);

  void checkState(Cells myCell, int state);

  void keepState(Cells cell);

  void moveState(Cells cell);
}
```
```java
public void setInitialParameters(Cells cell, Grid grid, List<Cells> neighbors, ResourceBundle statesBundle,
      Map<String, String> parameters){
    myGrid = grid;
    myStatesBundle = statesBundle;
  }

public void checkState(Cells cell, int state){
        Map<Integer, Consumer<Integer>> intMap = Map.of( Integer.parseInt(myStatesBundle.getString(SAME)), integers -> keepState(cell),
        Integer.parseInt(myStatesBundle.getString(MOVE)), integers -> moveState(cell)
        );
        consumerNextState(state, intMap.get(state));
        }

public void moveState(Cells cell){
        Map<Integer, Consumer<Integer>> intMap = Map.of(Integer.parseInt(myStatesBundle.getString(A)), integers -> moveCells(cell),
        Integer.parseInt(myStatesBundle.getString(B)), integer -> moveCells(cell),
        Integer.parseInt(myStatesBundle.getString(EMPTY)), integer -> keepState(cell));
        consumerNextState(cell.getCurrentState(), intMap.get(cell.getCurrentState()));
        }
        
public void keepState(Cells cell){
        cell.setMyNextState(cell.getCurrentState());}
```
The Interface-Segregation Principle basically states that classes should not depend on interfaces that they do not need to use. This is followed well in our code. For the SchellingSegregation and WaTor games, there is an added
functionality in which cells do not just change state - they move to an empty cell. Earlier, this was implemented within the model itself, but this caused a lot of duplicated code. Basically, though none of the other games needed any sort of movement,
both SchellingSegregation and WaTor implemented the same structure of code to move cells (each just had its own conditions to satisfy). Since both classes implemented the same methods in different ways, and none of the other games required these methods, it made most sense to separate these
methods into a CellSocietyMovement interface. As we can see above, these methods were the checkState(), keepState(), and moveState() methods. Depending on the rules of the respective game, SchellingSegregation and WaTor would implement these differently. With the interface, all the duplicated code was taken out of the model, and only the classes that needed these methods used them.
I think this is a good example of Interface-Segregation because the alternative solution to reducing the method duplication would have been to make checkState(), moveState() and keepState() methods within the abstract CellSocietyModel class. Not only would this break the Interface-Segregation principle (games such as GameOfLife do not need this interface at all), but it would also cause Code Smells and violation of Single Responsibility Principle - the model should just know the next state of the cell, the movement should happen separately. Therefore, I think extracting the cell movement within the model to instead be an interface allowed for satisfaction of not only Interface-Segregation, but also clean code and Single Responsibility Principle.



### Code Design Examples

#### Made easier to implement by the code's design

* Feature: Neighboring Policies

* Justification: 
```java
 public abstract List<Cells> generateNeighbors(int row, int col, Grid myGrid);
```

```java
public List<Cells> generateNeighbors(int row, int col, Grid myGrid) {
    List<Cells> myCells = new ArrayList<>();
    int[] yChanges = new int[]{-1, 1};
    for (int j : yChanges) {
      if (colIsValid(col + j, myGrid)) {
        myCells.add(myGrid.getCell(row, col + j));
      }
    }
    return myCells;
  }
```
It was very straightforward to implement different types of Neighboring policies as a result of the design of Neighbors for this project.
Our team created an abstract Neighbors class that had an abstract generateNeighbors() method (seen above). Each of the extended Neighbor type classes would just override this method with their own implementation of it.
Above, we can see the abstract method, as well as the implementation of the method in the LeftRightNeighbors class. For each new Neighbor type that the team wanted to add, the only thing we had to change was to create an extended class with its own version of this abstract method. Currently, in our program we have 5 different types of fully functional Neighboring policies. If we wanted to add a new Neighboring policy, say "I-Shape" neighbors that would just want the top 3 and bottom 3 neighbors of a cell, the only thing we would need to do is create an IShapeNeighborsCell and create its own implementation of the generateNeighbors() method. This would just require a little bit of mathematical thinking, but would work instantly when the generateNeighbors() method was implemented.


#### Good Example **teammate** implemented
```java
RULE1=< 2 DEAD
RULE3=eq 3 ALIVE
RULE4=> 3 DEAD
```
```java
<=LessThan
>=GreaterThan
eq=Equal
```
```java
protected void initializeMyRules(){
    translationBundle = initializeBundle(ruleResourceBundleBase, "TranslationRules");
    for (String eachKey : ruleBundle.keySet()){
      String ruleString = ruleBundle.getString(eachKey);
      String[]ruleSet = ruleString.split(" ");
      runThroughRules(ruleSet);
    }
  }
```
```java
protected void runThroughRules(String[] ruleSet){
    Class [] paramTypesSub = {int.class, Integer.class};
    Object [] paramValuesSub = {Integer.parseInt(ruleSet[1]), Integer.parseInt(valueBundle.getString(ruleSet[2]))};

    try{
      String ruleBundleBase = "cellsociety.rule.Rule";
      Rule myRule = (Rule) Class.forName(
              String.format("%s%s", ruleBundleBase, translationBundle.getString(ruleSet[0]))).
          getConstructor(paramTypesSub).newInstance(paramValuesSub);
      myRules.add(myRule);
    }
    catch (Exception e){
      myErrorFactory.updateError(RULE_ERROR);
    }

  }
```
* Design: Property Bundle Usage to iterate through Rules

* Evaluation: One of my team members created a really cool way to deal with the rules within each of the CellSociety games. They noticed that each of the rules for all of the CellSociety games could be simplified down to LessThan, GreaterThan, and Equal to rules. With this realization, we were able to make a Rule class that was extended by these three conditions. Then, in terms of the rules for each individual game, we simply needed to pass the rule we wanted to consider (<, >, eq : translations seen above), the parameter to compare to (second value in the first snippet above), and what to return if the parameter matched the given rule condition (third parameter in the first snipped above). So if we break this down for the first line in the GameOfLifeRules property file above it would look like this:
   < = LessThan (use Reflection to call the RuleLessThan class)
   2 = number to compare to (in this case we will compare the number of alive neighbors the current cell has to 2)
   DEAD = the state to return if the comparison is met (if the number of alive neighbors is less than 2, a DEAD state will be returned to the model)

To me, this is a really efficient and easy way to parse through all the rules for every game. We just define the rules within a property file, and run through them to find which rule is met. I think this cuts down on a lot of unnecessary code that would have existed had we implemented the less than, greater than, and equal to rules for each game individually. It also is very easy to read and follows clean code principles. I think it also takes a lot of responsbility out of the CellSocietyRules class, which is good for Single-Responsibility Principle.


#### Needs Improvement Example **teammate** implemented
```java
private void addHistogram() {
    try {
      root.setLeft(setupHistogram());
      histogramAdded = true;
      barChartAdded = false;
    } catch (Exception e) {
      Alert error = myFactoryComponents.createErrorMessage("InvalidGame", "InvalidGameMessage");
      error.show();
    }
  }
```
```java
private VBox setupHistogram() {
    VBox vbox = new VBox();
    if (root.getTop().getId()=="MainPane") {
      vbox.setId("HistogramPane");
    }
    LineChart<Number, Number> histogram = myFactoryComponents.makeHistogram("CellStatesOverTime", setupHistogramXAxis(), setupHistogramYAxis());
    histogram.getData().add(series0);
    histogram.getData().add(series1);
    histogram.getData().add(series2);
    histogram.getData().add(series3);
    histogram.setLegendSide(Side.LEFT);
    vbox.getChildren().add(histogram);
    return vbox;
  }
```
* Design: Histogram additions to CellSocietyView class

* Evaluation: Something that I think could be improved is how Histograms are initialized and dealt with in the CellSocietyView class. I think the CellSocietyView class's role is to display the current states of all the cells in the grid, and to call the right classes to do so. There is a good usage of Single-Responsibility Principle throughout the View code as there are FactoryComponents for all of the JavaFX UI elements and a GridView class to actually deal with each individual cell and how it is displayed in the Grid. There is also a CellSocietyViewComponents class that has methods like setUpBottomPanel() and setUpColorOptions() that set up some of the additional UI features that exist in the game (like the ability to change the background color). I think these are really well made, but when it comes to the Histogram, this already existing architecture is not used. Since the Histogram is an additional feature outside of the cell grid and states itself, I think that it should be dealt with in the CellSocietyViewComponents class - just as the About Section and Color Options are. Having it within the high-level CellSocietyView class makes the CellSocietyView class no longer follow Single-Responsibility Principle, and it also is somewhat hard to follow. I think had the methods above been moved to the CellSocietyViewComponents class, the high-level CellSocietyView class would be able to follow Single Responsibility, and have cleaner code. I think this change would also allow for extensions later on, for example if one wanted to add more features to the histogram it would make more sense to alter the method corresponding to histograms in the CellSocietyViewComponents class versus adding more lines to the high-level CellSocietyView class.




## Your Design

### Design Challenge

* Trade-offs: For me, the biggest design challenge I dealt with was the WaTor game. I noticed early on that the game was very different in functionality than all the other games, though it shared the movement action with the SchellingSegregation game. To combat this design issue, I created the CellSocietyMovement interface (as described above), and also created a WaTor developer class to be used in conjunction with the WaTorMovement class that extended CellSocietyMovement.

* Alternate designs: After implementing the CellSocietyMovement interface for SchellingSegregation and WaTor, I noticed that the WaTorMovement class was very long (above 300 lines!). I had two options at this point: I could keep the design as is because the code was 100% functional, or I could try to separate out some of the unique WaTor functionality that was not addressed within the CellSocietyMovement interface. 

* Solution: The second option made more sense to me, so I created a WaTorDeveloper class. This class was used as an object within the WaTorMovement class, and it basically dealt with all of the unique WaTor functionality outside of cell movement (it dealt with the energy for Sharks and reproduction for Fish and Sharks). Each time the WaTorMovement class was called, it would update and check all of the energy and reproduction levels, and then move first as described by the returned value from the rules class, and then if movement was required due to a cell reproducing or dying out. The WaTorDeveloper class would check the energy and reproduction when called by the WaTorMovement class, so the WaTorMovement would also update any movement immediately as returned from the developer class. 


* Justification or Suggestion: I am fairly happy with how I implemented this. I think the CellSocietyMovement follows the interface-segregation principle well in that it takes away unnecessary methods from the Model and instead only applies them to the games that need it. The WaTorDeveloper class goes a step further and only implements WaTor specific considerations (energy and reproduction). I think this follows single-responsibility well. However, I think that in the future I would want to make a Developer interface, such that if any new games were added with similar features to WaTor, instead of creating its own Developer class with similar methods to WaTor, I could just implement the Developer interface so that it could use similar methods in different ways. 


### Code Design Examples

#### Good Example **you** implemented
```java
public void moveState(Cells cell){
    Map<Integer, Consumer<Integer>> intMap = Map.of(Integer.parseInt(myStatesBundle.getString(A)), integers -> moveCells(cell),
        Integer.parseInt(myStatesBundle.getString(B)), integer -> moveCells(cell),
        Integer.parseInt(myStatesBundle.getString(EMPTY)), integer -> keepState(cell));
    consumerNextState(cell.getCurrentState(), intMap.get(cell.getCurrentState()));
  }
```
```java
  private void consumerNextState(int currentState, Consumer<Integer> consumer){
    consumer.accept(currentState);
  }
```
* Design: Use of consumers throughout the code to reduce if/else blocks and make code cleaner. In order to determine the next state of a cell given its current state and the result of what its next state should be after running it through the game rules.

* Evaluation: Throughout the code (including the above example), I think that I made good use of lambda functions/consumers to reduce Code Smells and if/else blocks. In this program, depending on what cell state was returned from the rules classes, a cell's next state would be set. For example, in the snippets above for SchellingSegregation, the rules class would have returned a "MOVE" state, meaning that the cell would need to move to an empty cell. Now this move would only be valid if the current cell was already filled with some type (either Group A or Group B). If the cell was already empty and surrounded by mostly non-empty cells, it still wouldn't move as empty cells are not considered for cell movement. The above consumer set up checks to see what state the current cell is (by reading the valid key from the game's state properties file). Given the state, it either calls the moveCells() method to move the cell to an empty state, or in the case of an already empty cell it calls the keepState() method to keep the cell as is. This, in my opinion, is a very clean way of considering these conditions. Without consumers the code would have looked like this:
```java
public void moveState(Cells cell){
    if(cell.getCurrentState() == Integer.parseInt(myStatesBundle.getString(A) ||
        cell.getCurrentState() == Integer.parseInt(myStatesBundle.getString(B))){
        moveCells(cell);
    }
    else if(cell.getCurrentState() == Integer.parseInt(myStatesBundle.getString(EMPTY))){
        keepState(cell);
        }
}
```
This is much denser code than is seen with the use of consumers. I think utilizing consumers throughout the program cut down on a lot of unnecessary conditionals, and allowed the code to be more readable and clean.


#### Needs Improvement Example **you** implemented
```java
  private static final String FIRE = "Fire_Spread";
  private static final String INVALID_GRID = "InvalidGrid";
  private static final String WA_TOR = "wa_tor";
  private static final String PERCOLATION_STATES = "0,1,2";
  private static final String WA_TOR_STATES = "0,1,2";
  private static final String FIRE_STATES = "0,1,2";
  private static final String GAME_OF_LIFE = "game_of_life";
  private static final String GAME_OF_LIFE_STATES = "0,1";
  private static final String PERCOLATION = "percolation";
  private static final String SCHELLING_SEGREGATION = "schelling_segregation";
  private static final String SCHELLING_SEGREGATION_STATES = "0,2,3";
```
```java
public GridFactory(){
    gameStates = Map.of(WA_TOR, WA_TOR_STATES,
        FIRE, FIRE_STATES,
        GAME_OF_LIFE, GAME_OF_LIFE_STATES,
        PERCOLATION, PERCOLATION_STATES,
        SCHELLING_SEGREGATION, SCHELLING_SEGREGATION_STATES);
  }
```
* Design: Setting up possible state values for each game to be used to initialize a grid in the case of a CSV filled with "x". If a CSV is loaded that has "x" in the place of state values for a specific game, the GridFactory() class uses the known possible states for each game to randomly generate an initial grid with valid starting states.

* Evaluation: I implemented a way to deal with CSV configuration files that did not already have the initial states of each cell in a grid defined. Basically, given the knowledge of what the valid states for the game type being played were, the GridFactory() class would call the Grid() class to initialize cells within those states randomly. This works really well, but I do not like how I implemented it. Truthfully, this was a feature that was added last minute while debugging the rest of the program, but I did not implement it well enough. I am mostly disappointed in the above two snippets. Basically, given the game type corresponding to the loaded CSV file, if the cell states were all "x", I would read the map defined in the GridFactory() constructor to find the valid game, and return its possible states. This is fine as there are no "magic values", but this would have been much neater if it was implemented in property files instead. I would have the keys equal to the states (for example, in the property file I would have: Fire_Spread=0,1,2). Then I would figure out what type of game the CSV file corresponding to, and then traverse through the property file to find the same key, and return all the state values. This would cut down on a lot of code and make the class much more readable. This would also allow for better extension, as if a new game was added only the property file would need to be changed. The code would not be touched at all (versus in the current implementation we would need to add a new key, new value, and add that pair to the map). This would be an extremely quick fix, but would make the design much neater for the different configuration file types.



## Conclusions

#### What part(s) of the code did you spend the most time on?
For me the longest time was spent on implementing the WaTor game, both in terms of understanding it conceptually, and figuring out how to design it well within the existing design. It took me some time to understand how the WaTor game worked in the first place, as there were multiple states (EMPTY, FISH, SHARK) and multiple rules for each individual state. This was different from the previous games because each state had the same basic set of rules (it changed based on its neighbor). In WaTor, however, a FISH would change differently than a SHARK would. I felt good about the design that we had coming into this developing WaTor, so I wanted to find out a way to keep that design, but also create a way to design WaTor well. I figured out how to do this be implementing a CellSocietyMovement interface, which was used by games requiring movement (such as WaTor). In this way, I was able to keep the initial model set-up, but just create a new addition that used an interface to deal with differences in states for specific games. This worked out well because the SchellingSegregation game also required movement, so I was able to refactor it to utilize this interface instead of having an extremely long model class.
#### How has your perspective on and practice of coding changed?
I think about ways to make as much of my code abstract as I can. Wherever there is an opportunity for abstraction and extension, I try to implement it. I think learning about interfaces has been really helpful too, because now I can use a basic abstraction to set up similarities, but if there are similarities between 2 games and not the others, I can simply create interfaces to be implemented in those cases, instead of making my extended classes too long. This is a really helpful tool to have going into the final project.
#### What specific steps can you take to become a better designer?
I think that I still need to brush up on making sure I follow some principles better, like the Liskov-Substitution Principle. It is easy to create abstractions that follow the Open-Close Principle, and then get lost adding independent features within extended classes. Once these extended classes start to diverge from abstractions too much, the Liskov principle is no longer validated. The application my team created worked, but had we substituted a class like SchellingSegregationModel to replace the CellSocietyModel class, it would have broke the program. This is something I really need to think about coming into the last project, so that I utilize more abstractions and interfaces in the case of differences, so that I follow SOLID principles.