# Journal : OOGA Team Review
## Team Number 3
## Name: Reya Magan
## Date: 12/7/2021


### Contributions and Roles

* Describe your role(s) and your satisfaction

I was primarily on the backend, pair-programmed on Gomoku game and implemented Othello Human and CPU. Created a reflection factory
and set up class. Pair-programming on components and players. I'm happy with this, but I think I let myself get really stressed this project
and made things that I had found simple earlier on in the semester much more difficult. I think I completed a good project, but I feel I could have
done more but out of stress blocked my min a bit. But I think what I did was still valuable to the team.

* Describe your team mate(s) role(s) and their satisfaction

Norah: Primarily on the backend, pair-programmed on Gomoku game and implemented Checkers Human and CPU. Created NeighborCells class to
  get neighbors of pieces within grid. Pair-programming on components and players. I think Norah was satisfied, checkers is a very complex game and she was able to get it implemented for Human and
CPU players quite well.

Nate: Primarily on the backend, pair-programmed on Gomoku game and implemented Connect 4 Human and CPU. Pair programmed on
components and players. Developed logging across project. Nate learned logging which was really cool, I think he had wanted to spend some more time overall though.

Steven: Primarily on Controller and View. Worked on GameController as well as connections between GameView and model. Worked on all the parsing and data file components. Created packets, simrules,
and csvrules for parsing of files and data collection. Created view elements and displays. Worked on cheat keys. Steven was satisfied as he implemented really cool concepts like property observers, and 
was an overall great team player.

Justin: Primarily on Controller and View. Worked on MainController as well as connections between MainController, MainView, and GameController. Created view elements and displays. Implemented CellView and GridView
for each piece and the general game board. Worked on cheat keys. Justin was satisfied as he was well knowledgeable and kept everyone on track.



* Roles beyond coding you took on and their benefit

I did a lot of organization for this project, as I packaged all the public methods into interfaces so that a third party individual would be able to easily understand them. I also participated in all the discussions, even for code that I was not involved in, so that if the time came where I needed to hop onto something I did not work on, I would be familiar with the content. This came in handy as our APIs are very clear now, and I ended up helping out with the MainView as well for better functionality and aesthetics.

* People who generally took on work "between assigned responsibilities" and outcome for the team

Steven was willing to help out with multiple aspects of the code, I had ideas to refactor the model and he helped me articulate and plan this out. I really appreciated that.

There was a bug with the Othello CPU that had kept me up for hours, and I asked Norah if she could take a look. She took a pause from her work to help me out, and together we were able to solve the issue.


### Team Events

* Describe when you felt the most satisfaction

On Saturday Dec 6th, our entire team spent the whole day in the study room in a hackathon style environment. We started the day with 4 somewhat working games with Human players, and ended the day with almost every game working with both Human
and CPU players. We all worked together very well and helped each other to achieve this, so I'm very proud of that.

* Describe when you felt the team made the biggest leap ahead

It took some time for the backend and frontend to get connected, but around the first few days back from Thanksgiving break we got one game connected fully (in human implementation) from backend to frontend. This was really exciting and I felt that the other
three games were much simpler as a result.

* Describe way(s) the team has worked well

I think every person on the team was respectful and willing to help. We all made our best efforts to communicate. I think of all my teams, this team was the most connected. I have had issues with teams not communicating in the past, but this time I felt that I could actually have confidence that everyone would get their part done.

* Describe problem(s) with the way the team worked together

I think with a team that has all its members being quite design-centered and good at communication, the tradeoff is that there are bound to be long discussions. This was definitely the case as we got lost in the weeds a lot, and sometimes
people were not willing to compromise. I think our final design is good, but I truthfully believe that more compromise could have made it really good.

### Learning Opportunities

* Your biggest strength as a teammate

I always get my work done, and am always on top of communication. There are times where I struggle to grasp concepts, but I work
hard to remain a good partner and to learn new things.

* Your biggest weakness as a teammate

I take on a lot of (self-imposed) stress that mentally blocks me at times. I don't think this impacted my work, but I think I took longer
on some things than I normally would have. Looking back, I think I was a good partner who did all the required work and was always willing to help,
but I wish I had been a little bit quicker at things.

* Something you have learned about team collaboration/communication

After having a team that barely communicated to one that communicated so much that discussions never ended, I've learned that it is important
to maintain a balance for everything. In my previous project I worried I was being pushy, and this time I worried that I could have been more open.
Looking back though, there was no correct way to deal with things. It is important to understand your team and react accordingly.
When I noticed this team had a lot of discussions, I tried to be more open to new ideas. I admit there were times that I did not agree with my teammates and felt
fairly strongly about a topic, but I think overall I was willing to compromise a lot.

* A specific thing you can do to ensure successful team outcomes in the future

Being more open in terms of pushing myself. I've learned from this class that I am capable of being a good software engineer and team player.
I think my biggest weakness is how quickly I psych myself out - it can affect both efficiency and teamwork. But I think I have improved a lot
from the first project. In the OOLALA project I had some individual ideas, but often elaborated and went further on ideas proposed by my partner. Now,
I feel comfortable having design discussions, and I feel confident that I have something meaningful to add to the conversation. I think I need to continue 
to improve on this, so that I can become a better software engineer and team player. I have really enjoyed this class, and have grown both as a programmer and 
as an individual.