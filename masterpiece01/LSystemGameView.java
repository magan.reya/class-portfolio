package oolala.view;

import java.io.FileNotFoundException;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import oolala.view.factories.ImageFileFactory;
import oolala.view.factories.SystemFactory;

import java.io.File;
/**
 * This class extends the GameView class to add extra JavaFX elements that are unique only to
 * the LSystem Application. It creates these elements using factories and event calls.
 *
 * I chose this to show for an example of extension for my masterpiece. It inherits all the common features from the
 * abstract GameView, and then implements a unique feature to addAnimals at the end of a drawing which is unique to LSystem.
 */
public class LSystemGameView extends GameView {

    private ImageFileFactory myImage = new ImageFileFactory();
    private SystemFactory mySystem = new SystemFactory();

    /**
     * Constructor for LSystemGameView. It has the same parameters passed in as GameView
     * @param myGame: LSystem
     * @param myLanguage: selected Language to play in
     */
    public LSystemGameView(String myGame, String myLanguage) {
        super(myGame, myLanguage);
    }

    @Override
    protected Node makeVerticalPanel() {
        VBox right = (VBox) super.makeVerticalPanel();
        Button addAnimals = getMyButton().makeButton(getButtonDisplays(), "AddAnimal", event -> addAnimal());
        Label instructions = getMyTextField().makeTitle(getTitleDisplays(), "LSystem");
        right.getChildren().addAll(addAnimals, instructions);
        return right;
    }

    /**
     * Abstract method from GameView. For LSystem, the user is prompted to enter a list of parameters (length, angle,
     * numLevels) to be used within the game
     * @return comma-separated string of parameters
     */
    @Override
    public String setUpSystem() {
        return mySystem.createSystem(getTitleDisplays(), getInputDisplays().getString("LSystem"), getErrorDisplays().getString("LSystem"));
    }

    private void addAnimal() {
        FileChooser imgFile = myImage.changeImage(getInputDisplays().getString("AddImage"));
        try {
            File imgFileString = imgFile.showOpenDialog(new Stage());
            String[] animalName = imgFileString.getName().split("\\.");
            getMyAnimalDisplay().addImageToDrawing(animalName[0]);
        } catch (Exception e) {
            return;
        }
    }
  }

