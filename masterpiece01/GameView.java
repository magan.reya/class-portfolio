package oolala.view;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import oolala.controller.GameController;
import oolala.model.AnimalData;
import oolala.view.events.ColorEventCall;
import oolala.view.events.CommandEventCalls;
import oolala.view.events.ErrorPopUps;
import oolala.view.events.HomeEventCall;
import oolala.view.events.NewGameEventCalls;
import oolala.view.factories.*;

import java.util.*;

/**
 * This class is the central view class. It sets up the scene entirely with the relevant nodes being
 * created using factories and then being mapped to relevant ActionEvents. The view changes dynamically
 * as the model changes (model -> controller -> view). This is an abstract class that sets up all of the
 * common JavaFX elements that are used across the Logo, LSystem, and Darwin games.
 *
 * I chose this class as part of my masterpiece because I think it follows the open-close principle well, since
 * this is an abstract class, it implements basic elements that are seen across all the games within OOLALA. All
 * of the unique features then are implemented in their respective game view classes. The abstraction allows me to add
 * new GameViews easily without needing to change this abstract class at all.
 *
 * Note: Lines 172 - 197 were not written by me, and I do not necessarily agree with their implementation. They were
 * added toward the end of the project, at which point there was not enough time to fix them. I would have
 * wanted to use factories and event call classes to represent them. My TA mentioned that I should
 * include this information here for consideration when my design is looked at. Thank you!
 */
public abstract class GameView{


  private GameController myController;
  private Group gameDisplay;
  private ComboBox<String> myHistory;
  private TextField myAddedCommands;
  private String mySelectedGame;
  private String mySelectedLanguage;
  public Timeline myAnimation;
  private static final int TITLE_OFFSET = 40;
  private static final int BORDER_OFFSET = 20;
  private static final int BLOCK_SIZE = 400;
  private static final String RESOURCE_PACKAGE = "oolala.view.resources.";
  private static final String TITLE_DISPLAY_RESOURCE_BUNDLE = "TitleDisplay";
  private static final String INPUT_DISPLAY_RESOURCE_BUNDLE = "InputDialogTextDisplay";
  private static final String ERROR_DISPLAY_RESOURCE_BUNDLE = "ErrorTextDisplay";
  private static final String BUTTON_DISPLAY_RESOURCE_BUNDLE = "ButtonTextDisplay";
  private static final String COMBO_DISPLAY_RESOURCE_BUNDLE = "ComboTextDisplay";
  private static final String COLOR_DISPLAY_RESOURCE_BUNDLE = "LineColors";
  private static final String RESOURCE_FOLDER = "/" + RESOURCE_PACKAGE.replace(".", "/");
  private static final String STYLESHEET = "default.css";
  private final AnimalDisplay myAnimalDisplay;
  private final ButtonFactory myButton;
  private final ComboFactory myComboBox;
  private final TextFactory myTextField;
  private final NewGameEventCalls myNewGameEvents;
  private final CommandEventCalls myCommandEvents;
  private final HomeEventCall myHomeEvent;
  private final ResourceBundle titleDisplays;
  private final ResourceBundle inputDisplays;
  private final ResourceBundle errorDisplays;
  private final ResourceBundle buttonDisplays;
  private final ResourceBundle comboDisplays;
  private final ResourceBundle colorOptions;
  private final ErrorPopUps errorPopUps;
  private Rectangle background;
  private ColorEventCall myColorEvent;

  /**
   * This is the constructor that sets up the GameView. It initializes all of the factories, event calls,
   * and resource bundles that are used across the entire view package.
   * @param myGame: This is the selected Game (Logo, LSystem, Darwin) that is currently in play
   * @param myLanguage: This is the selected Language (English, Spanish, French) that the game is being played in
   */
  public GameView(String myGame, String myLanguage) {
    mySelectedGame = myGame;
    mySelectedLanguage = myLanguage;
    myAnimalDisplay = new AnimalDisplay();
    myButton = new ButtonFactory();
    myComboBox = new ComboFactory();
    myTextField = new TextFactory();
    myNewGameEvents = new NewGameEventCalls();
    myCommandEvents = new CommandEventCalls();
    myHomeEvent = new HomeEventCall();
    myColorEvent = new ColorEventCall();
    errorPopUps = new ErrorPopUps();
    titleDisplays = ResourceBundle.getBundle(RESOURCE_PACKAGE + TITLE_DISPLAY_RESOURCE_BUNDLE + mySelectedLanguage);
    inputDisplays = ResourceBundle.getBundle(RESOURCE_PACKAGE + INPUT_DISPLAY_RESOURCE_BUNDLE + mySelectedLanguage);
    errorDisplays = ResourceBundle.getBundle(RESOURCE_PACKAGE + ERROR_DISPLAY_RESOURCE_BUNDLE + mySelectedLanguage);
    buttonDisplays = ResourceBundle.getBundle(RESOURCE_PACKAGE + BUTTON_DISPLAY_RESOURCE_BUNDLE + mySelectedLanguage);
    comboDisplays = ResourceBundle.getBundle(RESOURCE_PACKAGE + COMBO_DISPLAY_RESOURCE_BUNDLE + mySelectedLanguage);
    colorOptions = ResourceBundle.getBundle(RESOURCE_PACKAGE + COLOR_DISPLAY_RESOURCE_BUNDLE + mySelectedLanguage);
    myAnimalDisplay.setResources(errorDisplays, titleDisplays);
  }

  /**
   * This method starts the animation of the game. A speed value is passed, and every speed seconds the game
   * calls myController.step() to show changes within the view as commands are called.
   * @param speed: This is the speed at which the animation will change/update
   */
  public void start(double speed) {
    myAnimation = new Timeline();
    myAnimation.setCycleCount(Timeline.INDEFINITE);
    myAnimation.getKeyFrames().add(new KeyFrame(Duration.seconds(speed), e -> myController.step()));
    myAnimation.play();
  }

  /**
   * This method creates the general game scene. It calls methods such as makeUserInputs() and makeVerticalPanel
   * that organize all of the relevant JavaFX elements within the game into their respective locations within the scene.
   * @param width: This is the width of the scene
   * @param height: This is the height of the scene
   * @param background: This is the background color of the scene.
   * @return scene: JavaFX scene with all the created elements
   */
  public Scene makeScene(int width, int height, Paint background) {
    myNewGameEvents.selectGame(myController, mySelectedGame);
    BorderPane root = new BorderPane();

    root.setCenter(makeGameDisplay());
    root.setTop(makeUserInputs());
    root.setRight(makeVerticalPanel());
    root.setBottom(makeBottomPanel());

    Scene scene = new Scene(root, width, height, background);
    scene.getStylesheets()
        .add(getClass().getResource(RESOURCE_FOLDER + STYLESHEET).toExternalForm());
    return scene;
  }


  /**
   * This method updates information related to the ImageView object as commands are colled. This could be positional
   * data, whether the animal is seen or not, whether there is a stamp, and anything related to a command call.
   * @param newData: AnimalData object that contains all the relevant data for a specified animal ImageView.
   */
  public void update(AnimalData newData) {
    myAnimalDisplay.updateFromData(newData, gameDisplay);
  }

  private Node makeGameDisplay() {
    gameDisplay = new Group();
    setupGameArea(gameDisplay);
    return gameDisplay;
  }

  private void setupGameArea(Group parent) {
    background = new Rectangle(BORDER_OFFSET, TITLE_OFFSET, BLOCK_SIZE, BLOCK_SIZE);
    background.setFill(Color.WHITE);
    parent.getChildren().add(background);
  }

  protected Node makeUserInputs() {
    Button newGame = myButton.makeButton(buttonDisplays,"NewGame",
        event -> myNewGameEvents.newGame(myHistory, gameDisplay, myAnimalDisplay, myController));
    Button loadCommandFile = myButton.makeButton(buttonDisplays,"LoadCommandFile",
        event -> myCommandEvents.loadCommandFile(titleDisplays, errorDisplays, myHistory, inputDisplays, mySelectedGame,
            myController));
    Button setHome = myButton.makeButton(buttonDisplays,"SetBeginningPosition",
        event -> myController.setHomeWithNewValues(
            myHomeEvent.getHomeInputValues(titleDisplays, inputDisplays, errorDisplays)));
    Button setColor = myButton.makeButton(buttonDisplays, "SetColor",
            event -> myAnimalDisplay.setStrokeColor(getColorObjectFromString(myColorEvent.setColor(inputDisplays, colorOptions))));
    return new HBox(newGame, loadCommandFile, setHome, setColor);
  }

  private void setBackgroundColor(){
    Color colorObject = getColorObjectFromString(myColorEvent.setColor(inputDisplays, colorOptions));
   background.setFill(colorObject);
  }

  private Color getColorObjectFromString(String newColor) {
    if(colorOptions.getString("Black").equals(newColor)){
     return Color.BLACK;
    }
    if(colorOptions.getString("Red").equals(newColor)){
      return Color.RED;
    }
    if(colorOptions.getString("Green").equals(newColor)){
      return Color.GREEN;
    }
    if(colorOptions.getString("Blue").equals(newColor)){
      return Color.BLUE;
    }
    if(colorOptions.getString("Yellow").equals(newColor)){
      return Color.YELLOW;
    }
    if(colorOptions.getString("White").equals(newColor)){
      return Color.WHITE;
    }
    return Color.BLACK;
  }

  protected Node makeVerticalPanel() {
    myHistory = myComboBox.makeComboBox(comboDisplays,"History",
        event -> myCommandEvents.visitOldCommands(myHistory, myController));
    Button setBackground = myButton.makeButton(buttonDisplays,"SetBackground", event -> setBackgroundColor());
    return new VBox(myHistory, setBackground);
  }

  /**
   * This method displays errors as they are called from the controller.
   * @param message: This is the error message that was initiated in the controller.
   */
  public void displayError(String message) {
      errorPopUps.showError(titleDisplays, message);

  }

  protected Node makeBottomPanel() {
    TextField myAddedCommands = myTextField.makeTextField();
    myAddedCommands.setId("InsertCommands");
    myAddedCommands.setPromptText(inputDisplays.getString("InsertCommands"));
    Button addCommand = myButton.makeButton(buttonDisplays,"AddCommand",
        event -> myCommandEvents.addCommand(myHistory, myAddedCommands, myController));
    BorderPane bp = new BorderPane();
    bp.setCenter(myAddedCommands);
    bp.setRight(addCommand);
    return bp;
  }

  /**
   * This method sets up the system for Logo, LSystem, and Darwin. It is not very important for Logo and Darwin,
   * but it is called whenever LSystem is set up to initiate all of the relevant parameters.
   * @return String of parameters relevant to current game.
   */
  public abstract String setUpSystem();

  /**
   * This method sets the controller to be connected to the view, and also sends the game language to the
   * controller.
   * @param controller:This is the controller within the game to which the commands are sent to for parsing.
   *                  The controller is set and also receives the correct language from this method.
   */
  public void setMyController(GameController controller) {
    myController = controller;
    setControllerLanguage();
  }

  private void setControllerLanguage(){
    myController.setResources(mySelectedLanguage);
  }

  protected GameController getMyController() {
    return myController;
  }

  protected AnimalDisplay getMyAnimalDisplay() {
    return myAnimalDisplay;
  }

  protected ButtonFactory getMyButton() {
    return myButton;
  }

  protected CommandEventCalls getMyCommandEvents() {
    return myCommandEvents;
  }

  protected TextFactory getMyTextField(){return myTextField;}

  protected ComboBox<String> getMyHistory(){return myHistory;}

  protected ResourceBundle getTitleDisplays() {
    return titleDisplays;
  }

  protected ResourceBundle getInputDisplays() {
    return inputDisplays;
  }

  protected ResourceBundle getErrorDisplays() {
    return errorDisplays;
  }

  protected ResourceBundle getButtonDisplays(){
    return buttonDisplays;
  }



}
