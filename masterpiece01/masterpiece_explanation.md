This folder has my first masterpiece for the OOLALA project. For this project I was involved with the view,
for which I created abstractions in order to follow Single-Responsibility and Open-Close principles. I have attached
the GameView abstract class, as well as one of the classes extending it (LSystemGameView). For each of the JavaFX elements
created within these classes I implemented factories, so I have also attached an example of a factory (ButtonFactory) as well.
The code except for lines 172 - 197 in the abstract GameView class is what I would like to show and wrote, so my mentor TA mentioned I should
address this in the JavaDoc comments for the GameView class - it is mentioned there in more detail.