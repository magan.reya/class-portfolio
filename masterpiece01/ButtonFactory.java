package oolala.view.factories;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

import java.util.ResourceBundle;

/**
 * I chose this class as an example of a factory in my view design. I think this allows for good consideration
 * of the single-responsibility principle as the only job of a factory class is to create a specific JavaFX element.
 * It allows the view to only display the scene, without having to create elements itself.
 */
public class ButtonFactory {


    /**
     * This method/class makes all the Button nodes seen on the scene, and maps them to an ActionEvent. For example,
     * the New Game button triggers a NewGameEventCall when pressed.
     * @param myTextDisplays: This resource bundle has all of the titles for the buttons that are seen on the screen
     * @param label: This is the key that is used to find the relevant Button title within the myTextDisplays resource
     *             bundle.
     * @param handler: This is the ActionEvent that is triggered when a button is clicked
     * @return result: The button that has been created as a result of this factory.
     */
    public Button makeButton(ResourceBundle myTextDisplays, String label, EventHandler<ActionEvent> handler){
        Button result = new Button();
        result.setText(myTextDisplays.getString(label));
        result.setOnAction(handler);
        return result;
    }
}
